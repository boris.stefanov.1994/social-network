Feature: Authenticated User

  Background: Load Home Page
    When User open SocialNetwork HomePage

  Scenario: 01 Admin can edit user profile
    Given User logs in as "Arnold"
    When Admin opens Admin Page
    And Authenticated user search for profile "Sherlock"
    And Authenticated user clicks on Profile button
    When Admin clicks on button Edit Profile
    And User select Nationality United States
    And User select Rank Legendary Eagle Master
    And User select Map Inferno
    And User select Weapon M249
    And User writes new description - "Murdock...I am coming for you!"
    When Admin clicks on button Update
    Then Nationality should be "United States"
    Then Rank should be "Legendary Eagle Master"
    Then Map should be "Inferno"
    Then Weapon should be "M249"
    Then Description should be "Murdock...I am coming for you!"
    And User logs out
    When User logs in as "Sherlock"
    And User clicks on Account page
    Then Nationality should be "United States"
    Then Rank should be "Legendary Eagle Master"
    Then Map should be "Inferno"
    Then Weapon should be "M249"
    Then Description should be "Murdock...I am coming for you!"
    And User logs out

  Scenario: 02 Admin can edit and delete post
    Given User logs in as "James"
    And Authenticated user create post "Captain Slow"
    And User logs out
    When User logs in as "Arnold"
    And Admin opens Admin Page
    When Admin edit a post with "Admin edited post" message
    And refresh the page
    Then post text should be "Admin edited post"
    And Admin deletes post
    And User logs out

  Scenario: 03 Admin can edit and delete comment
    Given User logs in as "James"
    And Authenticated user create post "Grand Tour is the new Top Gear"
    And Authenticated user create comment for a post
    And User logs out
    When User logs in as "Arnold"
    And Admin opens Admin Page
    When page is scrolled down
    And Admin clicks on button Edit comment
    When Admin edit comment with "Fiat Panda 1.2i" and hit key Enter
    Then comment is edited with "Fiat Panda 1.2i"
    And Admin clicks on button Delete comment
    And Admin deletes post
    And User logs out

  Scenario: 04 Admin can delete user
    Given User logs in as "Arnold"
    When Admin opens Admin Page
    And Authenticated user search for profile "James2"
    And Authenticated user clicks on Profile button
    When Admin deletes User Profile
    And User logs out
    And User logs in as "James2"
    Then Page title should be "Login"





