Feature: Authenticated User

  Background: Load Home Page
    When User open SocialNetwork HomePage

  Scenario: 01 Registered user can edit his profile
    Given User logs in as "Rambo"
    And User clicks on Account page
    And User clicks button Edit profile
    And User select Nationality United States
    And User select Rank Legendary Eagle Master
    And User select Map Inferno
    And User select Weapon M249
    And User writes new description - "Murdock...I am coming for you!"
    When User clicks on button Update
    Then Nationality should be "United States"
    Then Rank should be "Legendary Eagle Master"
    Then Map should be "Inferno"
    Then Weapon should be "M249"
    Then Description should be "Murdock...I am coming for you!"
    And User logs out

  Scenario: 02 Registered users can connect and disconnect with each other
    Given User logs in as "Rambo"
    When Authenticated user search for profile "Rocky Balboa"
    And Authenticated user clicks on Profile button
    And Authenticated user sends friend request
    Then User logs out
    And User logs in as "Rocky Balboa"
    Then Friend request is displayed in Profile Page
    When Authenticated user accept friend request
    Then this new friend should appear in Friends list
    When Authenticated user clicks on button Unfriend
    Then this user should not be a friend anymore
    And User logs out

  Scenario: 03 Registered user can create public and private post
    Given User logs in as "James"
    When Authenticated user selects Private post option
    And Authenticated user create post
    Then Post text should match
    And User logs out
    When Non-friend user logs in
    Then Authenticated user can NOT see the private post of a stranger
    And User logs out
    When User logs in as "Elon Musk"
    Then Authenticated user can see the private post of a friend
    And User logs out
    When User logs in as "James"
    And User clicks on Account page
    And Authenticated user deletes newly created post

  Scenario: 04 Registered user can like and unlike post
    Given User logs in as "James"
    When Authenticated user selects Private post option
    And Authenticated user create post
    Then Post text should match
    And User logs out
    When User logs in as "Elon Musk"
    Then Authenticated user can see the private post of a friend
    Then likes counter of the post is "0"
    When Authenticated user likes post
    Then likes counter of the post is "1"
    When Authenticated user unlikes post
    Then likes counter of the post is "0"
    And User logs out
    When User logs in as "James"
    And User clicks on Account page
    And Authenticated user deletes newly created post
    And User logs out

  Scenario: 05 Registered user can create a comment for a post, like and unlike comment
    Given User logs in as "James"
    When Authenticated user selects Private post option
    And Authenticated user create post
    Then Post text should match
    And Authenticated user create comment for a post
    And User logs out
    When User logs in as "Elon Musk"
    Then Authenticated user can see the private post of a friend
    Then Authenticated user can see the comment for the post
    Then likes counter of the comment is "0"
    When Authenticated user likes comment
    Then likes counter of the comment is "1"
    When Authenticated user unlikes comment
    Then likes counter of the comment is "0"
    And User logs out
    When User logs in as "James"
    And User clicks on Account page
    And Authenticated user deletes newly created post
    And User logs out

  Scenario: 06 Registered user can expand more comments
    Given User logs in as "James"
    When Authenticated user selects Private post option
    And Authenticated user create post
    Then Post text should match
    When Authenticated user creates multiple comments: 3
    And refresh the page
    Then only two comments should be visible
    When Authenticated user clicks on button Show more comments
    Then all comments should be visible
    And User clicks on Account page
    And Authenticated user deletes newly created post
    And User logs out

  Scenario: 07 Post feed is automatically sorted in chronological order
    Given User logs in as "James"
    When Authenticated user selects Private post option
    And Authenticated user create post "post 1"
    Then Post text should match "post 1"
    And Authenticated user create post "post 2"
    Then Post text should match "post 2"
    When User clicks on Account page
    Then post "post 2" should be on the top of the feed
    And Authenticated user deletes newly created post
    Then post "post 1" should be on the top of the feed
    And Authenticated user deletes newly created post
    And User logs out




