Feature: Login

  Background: Load Login Page
    When User open SocialNetwork HomePage
    And click on Login page

  Scenario: Successful Login with valid credentials
    When User enters Username as "james" and Password as "password"
    And click on Login button
    Then Page title should be "News feed page"
    When User clicks on Log out button
    Then Page title should be "Counter-Strike Social"

    Scenario Outline: Login DD with invalid credentials
      When User enters Username as "<name>" and Password as "<password>"
      And click on Login button
      Then Page title should be "Login"
      And navigate to Home Page

      Examples:
      | name | password |
      | Elon | password |
      | Elon Musk | pass |
      |  |  |
      | Elon Musk |  |
      |  | password  |