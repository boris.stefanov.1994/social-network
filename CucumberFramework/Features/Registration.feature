Feature: Registration

  Scenario Outline: Registration DD with invalid credentials
    When User open SocialNetwork HomePage
    And click on Registration page
    When User enters Username as "<name>" and Password as "<password>" and email as "<email>"
    And click on Register button
    Then Page title should be "Registration Page"
    And navigate to Home Page

    Examples:
      | name | password | email |
      | John | password |       |
      |  |  |                   |
      | John |  | john.connor@skynet.com |
      | | password | john.connor@skynet.com |
      | John | | |
      | | password | |
      | | | john.connor@skynet.com |