Feature: UnauthenticatedUser

  Scenario: Unauthenticated user can navigate to HomePage
    When User open SocialNetwork HomePage
    Then heading is Counter-Strike Social

  Scenario: Unauthenticated user can NOT see profile data of registered users
    When User open SocialNetwork HomePage
    When User search for profile "Elon Musk"
    And click on Profile button
    Then Page title should be "Login"
    And navigate to Home Page