package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AccountPage {

    WebDriver ldriver;

    public AccountPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    By buttonEditProfile = By.xpath("/html/body/div[2]/div/div[1]/div/div[2]/button[3]");

    By ddNationality = By.id("nationalityValue");

    By ddRank = By.id("rankValue");

    By ddMap = By.id("mapValue");

    By ddWeapon = By.id("weaponValue");

    By fieldDescription = By.id("descriptionValue");

    public By buttonUpdate = By.xpath("//*[@id=\"Change profile settings\"]/div/div/div/div/div/div[13]/button");

    public By textRank = By.xpath("/html/body/div[2]/div/div[1]/div/div[1]/div/div[1]/p/span[3]");

    public By textNationlity = By.xpath("/html/body/div[2]/div/div[1]/div/div[1]/div/div[2]/p/span[3]");

    public By textWeapon = By.xpath("/html/body/div[2]/div/div[1]/div/div[1]/div/div[3]/p/span[3]");

    public By textMap = By.xpath("/html/body/div[2]/div/div[1]/div/div[1]/div/div[4]/p/span[3]");

    public By textDescription = By.xpath("/html/body/div[2]/div/div[1]/div/div[1]/div/div[5]/p[2]");

    By buttonDeletePost = By.xpath("//*[@id=\"post-header\"]/div/div[4]/button[1]");

    public By textPostAccount = By.xpath("//div[contains(@id,'postDiv')]//p[contains(@id,'text-')]");

    public void clickButtonEditProfile(){
        ldriver.findElement(buttonEditProfile).click();
    }

    public void selectNationality(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ldriver.findElement(ddNationality)).perform();
        Select select = new Select(ldriver.findElement(ddNationality));
        select.selectByVisibleText("United States");
    }

    public void selectRank(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ldriver.findElement(ddRank)).perform();
        Select select = new Select(ldriver.findElement(ddRank));
        select.selectByVisibleText("Legendary Eagle Master");
    }

    public void selectMap(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ldriver.findElement(ddMap)).perform();
        Select select = new Select(ldriver.findElement(ddMap));
        select.selectByVisibleText("Inferno");
    }

    public void selectWeapon(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ldriver.findElement(ddWeapon)).perform();
        Select select = new Select(ldriver.findElement(ddWeapon));
        select.selectByVisibleText("M249");
    }

    public void editDescription(){
        ldriver.findElement(fieldDescription).click();
        ldriver.findElement(fieldDescription).clear();
        ldriver.findElement(fieldDescription).sendKeys("Murdock...I am coming for you!");
    }

    public void clickUpdate(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ldriver.findElement(buttonUpdate)).perform();
        ldriver.findElement(buttonUpdate).click();
    }

    public void deletePost(){
        ldriver.findElement(buttonDeletePost).click();
    }


}
