package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {

    WebDriver ldriver;

    public AdminPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    By buttonAdminPage = By.xpath("/html/body/div[1]/div/a[4]");

    By buttonAdminEditProfile = By.xpath("/html/body/div[2]/div/div[1]/div/div[2]/button[2]");

    By buttonAdminUpdate = By.xpath("//*[@id=\"Sherlock\"]/i");

    By buttonEditPost = By.xpath("//button[contains(@onClick,'')]//i[contains(@class,'fas fa-screwdriver')]");

    public By fieldPost = By.xpath("//div[contains(@id,'post-content')]//input");

    By buttonDeletePost = By.xpath("//button[contains(@class,'w3-margin-left')]//i[contains(@class,'fas fa-trash-alt')]");

    By buttondeleteUser = By.id("6");

    public By textPost = By.xpath("//div[contains(@id,'postDiv')]//p");

    public By buttonEditCommentAdmin = By.cssSelector("div > div:nth-child(5) > button:nth-child(2) > i");

    By fieldComment = By.xpath("//div[contains(@class,' w3-round')]//input[contains(@id,'textComment')]");

   public By textEditedComment = By.xpath("//div[contains(@id,'comment-')]//div[contains(@class,'w3-round')]//p");

   By buttonAdminDeleteComment = By.xpath("//div[contains(@id,'comments-list')]//button[contains(@class,'w3-margin-left')]//i[contains(@class,'fas fa-trash-alt')]");

    public void openAdminPage(){
        ldriver.findElement(buttonAdminPage).click();
    }

    public void clickAdminEditProfile(){
        ldriver.findElement(buttonAdminEditProfile).click();
    }

    public void clickAdminUpdate(){
        ldriver.findElement(buttonAdminUpdate).click();
    }

    public void adminEditPost(String editedPostText) throws InterruptedException{
        ldriver.findElement(buttonEditPost).click();
        Thread.sleep(1000);
        ldriver.findElement(fieldPost).click();
        Thread.sleep(500);
        ldriver.findElement(fieldPost).clear();
        Thread.sleep(500);
        ldriver.findElement(fieldPost).sendKeys(editedPostText);
        Thread.sleep(500);
        ldriver.findElement(fieldPost).sendKeys(Keys.ENTER);
        Thread.sleep(1000);
    }

    public void adminDeletePost(){
        ldriver.findElement(buttonDeletePost).click();
    }

    public  void deleteUser(){
        ldriver.findElement(buttondeleteUser).click();
    }

    public void adminEditComment(String textEdited) throws InterruptedException{
        ldriver.findElement(fieldComment).clear();
        Thread.sleep(500);
        ldriver.findElement(fieldComment).sendKeys(textEdited);
        Thread.sleep(1000);
        ldriver.findElement(fieldComment).sendKeys(Keys.ENTER);
        Thread.sleep(1000);
    }

    public void adminDeleteComment(){
        ldriver.findElement(buttonAdminDeleteComment).click();
    }

}
