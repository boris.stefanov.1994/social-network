package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver ldriver;

    public HomePage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    public By buttonHomeLogin = By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[2]/a");

    By buttonHomeRegister = By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[1]/a");

    By fieldSearch = By.id("input");

    By buttonProfile = By.xpath("//*[@id=\"users-div\"]/div/div/div[3]/a/button");

    public void clickHomeLogin(){
        ldriver.findElement(buttonHomeLogin).click();
    }

    public void clickHomeRegister(){
        ldriver.findElement(buttonHomeRegister).click();
    }

    public void search(String user){
        ldriver.findElement(fieldSearch).click();
        ldriver.findElement(fieldSearch).clear();
        ldriver.findElement(fieldSearch).sendKeys(user);
    }

    public void clickProfileButton(){
        ldriver.findElement(buttonProfile).click();
    }

}
