package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    public WebDriver ldriver;

    public LoginPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }


    By fieldUsername = By.id("username");

    By fieldPassword = By.id("password");

   public By buttonLogin = By.xpath("//*[@id=\"regTable\"]/div/div/div/div/form/input");


    public void enterUsername(String uname){
        ldriver.findElement(fieldUsername).clear();
        ldriver.findElement(fieldUsername).sendKeys(uname);
    }

    public void enterPassword(String pwd){
        ldriver.findElement(fieldPassword).clear();
        ldriver.findElement(fieldPassword).sendKeys(pwd);
    }

    public void clickLogin(){
        ldriver.findElement(buttonLogin).click();
    }

}
