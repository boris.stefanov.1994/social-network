package PageObjects;

import Utilities.WaitHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProfilePage {

    WebDriver ldriver;
    WaitHelper waitHelper;

    public ProfilePage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
        waitHelper = new WaitHelper(ldriver);
    }

    By buttonLogout = By.xpath("/html/body//div//a[contains(@title,'Log out')]");

    By buttonAcount = By.xpath("/html/body/div[1]/div/a[2]");

    By fieldSearchProfile = By.id("input");

    By buttonProfile = By.xpath("//*[@id=\"users-div\"]/div/div[3]/a/button");

    public By divFriendRequest = By.xpath("/html/body/div[3]/div/div[3]/div[2]/div/div/div[1]/h4");

    public By textFriendRequest = By.xpath("/html/body/div[3]/div/div[3]/div[2]/div/div/div[2]/div[2]/h5");

    By buttonAccept  = By.xpath("//*[@id=\"3\"]");

   public By textFriend = By.xpath("//*[@id=\"friendsDiv\"]/div/div[2]/a/h4");

    By buttonUnfriend = By.xpath("//*[@id=\"3\"]");

    public By textNoFriends = By.xpath("//*[@id=\"friendsDiv\"]/div/p");

    By ddPostSecurity = By.id("postSecure");

    By fieldPost = By.id("postValue");

    public By textPostPrivate = By.xpath("//*[@id=\"post-content\"]/p[1]");

    By buttonPost = By.xpath("/html/body/div[3]/div/div[2]/div[1]/div/div/div[2]/div/div/form/button");

    By butonLikePost = By.xpath("//*[@id=\"post-content\"]/button[1]");

    public By likesCounter = By.id("likes-count");

    By fieldComment = By.id("commentValue");

    By buttonComment = By.xpath("//div[contains(@id, 'post-')]//button[contains(@onclick, 'writeComment')]");

    public By textComment = By.xpath("//div[contains(@id, 'comment-')]//p[contains(@id,'text-')]");

    By buttonLikeComment = By.xpath("//div[contains(@id, 'comment-')]//button");

    public By likesCounterComments = By.id("comment-likes-count");

    By buttonShowMoreComments = By.xpath("//div[contains(@id,'post-')]//button[contains(@id,'comment-')]");

    public By divCommentsList = By.xpath("//div[contains(@id,'comments-list')]");


    public void clickLogout(){
        waitHelper.WaitForElement(ldriver.findElement(buttonLogout), 10);
        ldriver.findElement(buttonLogout).click();
    }

    public void clickAccountPage(){
       ldriver.findElement(buttonAcount).click();
    }

    public void searchProfile(String name){
        ldriver.findElement(fieldSearchProfile).clear();
        ldriver.findElement(fieldSearchProfile).sendKeys(name);
    }

    public void clickButtonUsernameProfile(){
        ldriver.findElement(buttonProfile).click();
    }

    public void clickAccept(){
       ldriver.findElement(buttonAccept).click();
    }

    public void clickUnfriend(){
        ldriver.findElement(buttonUnfriend).click();
    }

    public void selectPrivatePost(){
        Select select = new Select(ldriver.findElement(ddPostSecurity));
        select.selectByVisibleText("Private");
    }

    public void createPost() throws InterruptedException{
        ldriver.findElement(fieldPost).clear();
        ldriver.findElement(fieldPost).sendKeys("Selenium created post with private security!");
        Thread.sleep(1000);
        ldriver.findElement(buttonPost).click();
    }

    public void LikePost(){
        ldriver.findElement(butonLikePost).click();
    }

    public void createComment() throws InterruptedException {
        ldriver.findElement(fieldComment).click();
        ldriver.findElement(fieldComment).clear();
        ldriver.findElement(fieldComment).sendKeys("Selenium created comment.");
        Thread.sleep(1000);
        ldriver.findElement(buttonComment).click();
        Thread.sleep(1000);
    }

    public void likeComment(){
        ldriver.findElement(buttonLikeComment).click();
    }

    public void createMultipleComments(int iterations) throws InterruptedException {
        for (int i = 1; i <= iterations; i++) {
            ldriver.findElement(fieldComment).clear();
            ldriver.findElement(fieldComment).sendKeys("comment " + i);
            Thread.sleep(1500);
            ldriver.findElement(buttonComment).click();
            Thread.sleep(500);
        }
    }

    public void clickShowMoreComments(){
        ldriver.findElement(buttonShowMoreComments).click();
    }

    public void createPostMessage(String message) throws InterruptedException{
        ldriver.findElement(fieldPost).clear();
        ldriver.findElement(fieldPost).sendKeys(message);
        Thread.sleep(1000);
        ldriver.findElement(buttonPost).click();
    }


}
