package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {

    WebDriver ldriver;

    public RegistrationPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    By fieldUsername = By.id("username");

    By fieldPassword = By.id("password");

    By fieldEmail = By.id("email");

    By buttonRegister = By.xpath("//*[@id=\"regTable\"]/div/div/div/div/form/input");

    public By textSuccessfulRegistration = By.xpath("/html/body/h2");

    public void enterUsername(String uname){
        ldriver.findElement(fieldUsername).clear();
        ldriver.findElement(fieldUsername).sendKeys(uname);
    }

    public void enterPassword(String pwd){
        ldriver.findElement(fieldPassword).clear();
        ldriver.findElement(fieldPassword).sendKeys(pwd);
    }

    public void enterEmail(String email){
        ldriver.findElement(fieldEmail).clear();
        ldriver.findElement(fieldEmail).sendKeys(email);
    }

    public void clickRegister(){
        ldriver.findElement(buttonRegister).click();
    }

    public String getMessageSuccessfulRegistration(){
        return ldriver.findElement(textSuccessfulRegistration).getText();
    }


}
