package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class UserProfilePage {

    WebDriver ldriver;

    public UserProfilePage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    public By buttonSendFriendRequest = By.id("4");

    public void clickButtonSendConnection(){
        ldriver.findElement(buttonSendFriendRequest).click();
    }


}
