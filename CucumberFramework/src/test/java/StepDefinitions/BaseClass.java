package StepDefinitions;

import PageObjects.*;
import Utilities.WaitHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class BaseClass {

    public WebDriver driver;
    WaitHelper waitHelper;
    public static Logger logger;
    public Properties configProp;

    public HomePage homePage;
    public LoginPage loginPage;
    public ProfilePage profilePage;
    public AccountPage accountPage;
    public AdminPage adminPage;
    public RegistrationPage registrationPage;
    public UserProfilePage userProfilePage;

    public void Delay(int milliseconds) throws InterruptedException {
        Thread.sleep(milliseconds);
    }

    public void Login(String name) throws InterruptedException {
        waitHelper = new WaitHelper(driver);
        Delay(1000);
        waitHelper.WaitForElement(driver.findElement(homePage.buttonHomeLogin), 10);
        driver.findElement(homePage.buttonHomeLogin).click();
        Delay(1000);
        driver.findElement(By.id("username")).sendKeys(name);
        Delay(1000);
        driver.findElement(By.id("password")).sendKeys("password");
        Delay(1000);
        driver.findElement(loginPage.buttonLogin).click();
        Delay(1000);
    }

    public void ScrollPageDown()throws InterruptedException{
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,350)");
        Delay(500);
    }

    public static String randomString(){
        String generatedString = RandomStringUtils.randomAlphabetic(5);
        return generatedString;
    }

    public void takeScreenShot(WebDriver driver, String tname) throws IOException {

        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        File target = new File(System.getProperty("user.dir") + "/Screenshots/" + tname + ".png");
        FileUtils.copyFile(source, target);
        System.out.println("Screenshot taken");
    }

}
