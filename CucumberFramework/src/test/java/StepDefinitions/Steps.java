package StepDefinitions;

import PageObjects.*;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import cucumber.api.java.hu.De;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Time;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class Steps extends BaseClass{

    @Before
    public void SetUp() throws IOException {

        //Reading properties
        configProp = new Properties();
        FileInputStream configPropfile = new FileInputStream("config.properties");
        configProp.load(configPropfile);

        //Added logger
        logger= Logger.getLogger("Counter-Strike: Global Offensive");
        PropertyConfigurator.configure("log4j.properties");

        String browser = configProp.getProperty("browser");

        //Multi-browser support
        if (browser.equals("chrome")){
            System.setProperty("webdriver.chrome.driver", configProp.getProperty("chromepath"));
            driver = new ChromeDriver();
        } else if(browser.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", configProp.getProperty("firefoxpath"));
            driver = new FirefoxDriver();
        }


        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void TearDown(){
        driver.quit();
    }


    //Feature: UnauthenticatedUser.feature ----------------------------------------------------------------------------------------

    @When("User open SocialNetwork HomePage")
    public void user_open_SocialNetwork_HomePage() {

        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
        profilePage = new ProfilePage(driver);
        accountPage = new AccountPage(driver);
        adminPage = new AdminPage(driver);
        registrationPage = new RegistrationPage(driver);
        userProfilePage = new UserProfilePage(driver);

        driver.get("http://localhost:8080");
        logger.info("*****Starting browser*****");
    }

    @Then("heading is Counter-Strike Social")
    public void i_verify_that_heading_is_HomePage() throws IOException {
        if (driver.getTitle().equals("Counter-Strike Social")) {
            assertEquals("Counter-Strike Social", driver.getTitle());
            logger.info("Asserting Page Title - pass");
        } else {
            takeScreenShot(driver, "Asserting Page Title - fail");
            assertEquals("Counter-Strike Social", driver.getTitle());
            logger.info("Asserting Page Title - fail");
        }
    }

    @When("User search for profile {string}")
    public void user_search_for_profile(String user) throws InterruptedException {
        homePage.search(user);
        Delay(1000);
        logger.info("User searches profile");
    }

    @When("click on Profile button")
    public void click_on_Profile_button() throws InterruptedException {
        homePage.clickProfileButton();
        Delay(1000);
        logger.info("Click on Profile button");
    }

    @And("close browser")
    public void close_browser() {
        driver.quit();
        logger.info("*****Closing browser*****");
    }
//Feature: Login.feature -----------------------------------------------------------------------------------------------

// Scenario: Login DD with invalid credentials steps

    @And("click on Login page")
    public void click_on_Login_page() {
        homePage.clickHomeLogin();
        logger.info("Click button Home Login");
    }

    @When("User enters Username as {string} and Password as {string}")
    public void user_enters_Email_as_and_Password_as(String username, String password) throws InterruptedException {
        Delay(500);
        loginPage.enterUsername(username);
        Delay(500);
        loginPage.enterPassword(password);
        logger.info("Entered username and password");
    }

    @And("click on Login button")
    public void click_on_Login_button() throws InterruptedException {
        loginPage.clickLogin();
        Delay(2000);
        logger.info("Click button Login");
    }

    @Then("Page title should be {string}")
    public void page_title_should_be(String title) throws IOException {
        if (title.equals(driver.getTitle())) {
            assertEquals(title, driver.getTitle());
            logger.info("Asserting Page Title - pass");
        } else {
            takeScreenShot(driver, "Asserting Page Title - fail");
            assertEquals(title, driver.getTitle());
            logger.info("Asserting Page Title - fail");
        }
    }

    @When("User clicks on Log out button")
    public void user_clicks_on_Log_out_button() throws InterruptedException {
        profilePage.clickLogout();
        Delay(1500);
        logger.info("Click button Log out");
    }

    @And("navigate to Home Page")
    public void navigate_to_Home_Page() throws InterruptedException {
        driver.navigate().to("http://localhost:8080/");
        Delay(1000);
        logger.info("Navigate to Home Page");
    }

//Feature: Registration.feature ----------------------------------------------------------------------------------------

// Scenario: Register DD with invalid credentials steps

    @When("click on Registration page")
    public void click_on_Registration_page() throws InterruptedException {
        homePage.clickHomeRegister();
        Delay(1000);
        logger.info("Navigate to Registration Page");
    }

    @When("User enters Username as {string} and Password as {string} and email as {string}")
    public void user_enters_Username_as_and_Password_as_and_email_as(String username, String password, String email) throws InterruptedException {
        registrationPage.enterUsername(username);
        Delay(500);
        registrationPage.enterPassword(password);
        Delay(500);
        registrationPage.enterEmail(email);
        Delay(500);
        logger.info("Entered username, password and email");
    }

    @When("click on Register button")
    public void click_on_Register_button() {
        registrationPage.clickRegister();
        logger.info("Click button Register");
    }

    //Feature: AuthenticatedUser.feature ----------------------------------------------------------------------------------------

    //Scenario: Registered user can edit his profile

    @Given("User logs in as {string}")
    public void user_logs_in__as(String username) throws InterruptedException {
        Login(username);
        logger.info("Logging in");
    }

    @When("User clicks on Account page")
    public void user_clicks_on_Acount_page() throws InterruptedException {
        profilePage.clickAccountPage();
        Delay(1000);
        logger.info("Navigate to Account Page");
    }

    @When("User clicks button Edit profile")
    public void user_clicks_button_Edit_profile() throws InterruptedException {
        accountPage.clickButtonEditProfile();
        Delay(1000);
        logger.info("Edit profile");
    }

    @When("User select Nationality United States")
    public void user_select_Nationality_United_States() throws InterruptedException {
        accountPage.selectNationality();
        Delay(1000);
        logger.info("Select nationality");
    }

    @When("User select Rank Legendary Eagle Master")
    public void user_select_Rank_Legendary_Eagle_Master() throws InterruptedException {
        accountPage.selectRank();
        Delay(1000);
        logger.info("Select rank");
    }

    @When("User select Map Inferno")
    public void user_select_Map_Inferno() throws InterruptedException {
        accountPage.selectMap();
        Delay(1000);
        logger.info("Select map");
    }

    @When("User select Weapon M249")
    public void user_select_Weapon_M() throws InterruptedException {
        accountPage.selectWeapon();
        Delay(1000);
        logger.info("Select weapon");
    }

    @When("User writes new description - {string}")
    public void user_writes_new_description(String string) throws InterruptedException {
        accountPage.editDescription();
        Delay(1000);
        logger.info("Edit description");
    }

    @When("User clicks on button Update")
    public void user_clicks_on_button_Update() throws InterruptedException {
        accountPage.clickUpdate();
        Delay(1000);
        logger.info("Click button Update");
    }

    @Then("Nationality should be {string}")
    public void nationality_should_be(String nationality) throws IOException {
        if (driver.findElement(accountPage.textNationlity).getText().equals(nationality)) {
            assertEquals(driver.findElement(accountPage.textNationlity).getText(), nationality);
            logger.info("Asserting nationality - pass");
        } else {
            takeScreenShot(driver, "Asserting nationality - fail");
            assertEquals(driver.findElement(accountPage.textNationlity).getText(), nationality);
            logger.info("Asserting nationality - fail");
        }
    }

    @Then("Rank should be {string}")
    public void rank_should_be(String rank) throws IOException {
        if (driver.findElement(accountPage.textRank).getText().equals(rank)) {
            assertEquals(driver.findElement(accountPage.textRank).getText(), rank);
            logger.info("Asserting rank - pass");
        } else {
            takeScreenShot(driver, "Asserting rank - fail");
            assertEquals(driver.findElement(accountPage.textRank).getText(), rank);
            logger.info("Asserting rank - fail");
        }
    }

    @Then("Map should be {string}")
    public void map_should_be(String map) throws IOException {
        if (driver.findElement(accountPage.textMap).getText().equals(map)) {
            assertEquals(driver.findElement(accountPage.textMap).getText(), map);
            logger.info("Asserting map - pass");
        } else {
            takeScreenShot(driver, "Asserting map - pass");
            assertEquals(driver.findElement(accountPage.textMap).getText(), map);
            logger.info("Asserting map - fail");
        }
    }

    @Then("Weapon should be {string}")
    public void weapon_should_be(String weapon) throws IOException {
        if (driver.findElement(accountPage.textWeapon).getText().equals(weapon)) {
            assertEquals(driver.findElement(accountPage.textWeapon).getText(), weapon);
            logger.info("Asserting weapon - pass");
        } else {
            takeScreenShot(driver,"Asserting weapon - fail");
            assertEquals(driver.findElement(accountPage.textWeapon).getText(), weapon);
            logger.info("Asserting weapon - fail");
        }
    }

    @Then("Description should be {string}")
    public void description_should_be(String description) throws IOException {
        if (driver.findElement(accountPage.textDescription).getText().equals(description)) {
            assertEquals(driver.findElement(accountPage.textDescription).getText(), description);
            logger.info("Asserting description - pass");
        } else {
            takeScreenShot(driver, "Asserting description - fail");
            assertEquals(driver.findElement(accountPage.textDescription).getText(), description);
            logger.info("Asserting description - fail");
        }
    }

    @Then("User logs out")
    public void user_logs_out() throws InterruptedException {
        profilePage.clickLogout();
        Delay(1000);
        logger.info("User logs out");
    }

    //Scenario: Registered users can connect and disconnect with each other

    @When("Authenticated user search for profile {string}")
    public void authenticated_user_search_for_profile(String profile) throws InterruptedException {
        profilePage.searchProfile(profile);
        Delay(1000);
        logger.info("Search profile");
    }

    @When("Authenticated user clicks on Profile button")
    public void authenticated_user_clicks_on_Profile_button() throws InterruptedException {
        profilePage.clickButtonUsernameProfile();
        Delay(1000);
        logger.info("Click button Profile");
    }

    @When("Authenticated user sends friend request")
    public void authencited_user_sends_friend_request() throws InterruptedException {
        userProfilePage.clickButtonSendConnection();
        Delay(1000);
        logger.info("Send friend request");
    }

    @Then("Friend request is displayed in Profile Page")
    public void friend_request_is_displayed_in_Profile_Page() throws IOException {
        if (driver.findElement(profilePage.divFriendRequest).isDisplayed()) {
            assertTrue(driver.findElement(profilePage.divFriendRequest).isDisplayed());
            assertEquals(driver.findElement(profilePage.textFriendRequest).getText(), "Rambo");
            logger.info("Assert friend request is sent - pass");
        } else {
            takeScreenShot(driver, "Assert friend request is sent - fail");
            assertTrue(driver.findElement(profilePage.divFriendRequest).isDisplayed());
            assertEquals(driver.findElement(profilePage.textFriendRequest).getText(), "Rambo");
            logger.info("Assert friend request is sent - fail");
        }
    }

    @When("Authenticated user accept friend request")
    public void authenticated_user_accept_friend_request() throws InterruptedException {
        profilePage.clickAccept();
        Delay(1000);
        logger.info("Accept friend request");
    }

    @Then("this new friend should appear in Friends list")
    public void this_new_friend_should_appear_in_Friends_list() throws IOException {
        if (driver.findElement(profilePage.textFriend).getText().equals("Rambo")){
            assertEquals(driver.findElement(profilePage.textFriend).getText(), "Rambo");
            logger.info("Friends list is updated - pass");
        } else {
            takeScreenShot(driver,"Friends list is updated - fail");
            assertEquals(driver.findElement(profilePage.textFriend).getText(), "Rambo");
            logger.info("Friends list is updated - fail");
        }
    }

    @When("Authenticated user clicks on button Unfriend")
    public void authenticated_user_clicks_on_button_Unfriend() throws InterruptedException {
        profilePage.clickUnfriend();
        Delay(1000);
        logger.info("Unfriend friend");
    }

    @Then("this user should not be a friend anymore")
    public void this_user_should_not_be_a_friend_anymore() throws IOException {
        if (driver.findElement(profilePage.textNoFriends).isDisplayed()) {
            assertTrue(driver.findElement(profilePage.textNoFriends).isDisplayed());
            assertEquals(driver.findElement(profilePage.textNoFriends).getText(), "You still don't have any buddies");
            logger.info("Asserting friends list - pass");
        } else {
            takeScreenShot(driver,"Asserting friends list - fail");
            assertTrue(driver.findElement(profilePage.textNoFriends).isDisplayed());
            assertEquals(driver.findElement(profilePage.textNoFriends).getText(), "You still don't have any buddies");
            logger.info("Asserting friends list - fail");
        }
    }

    //Scenario: Registered user can create public and private post

    @When("Authenticated user selects Private post option")
    public void authenticated_user_selects_Private_post_option() throws InterruptedException {
        profilePage.selectPrivatePost();
        Delay(500);
        logger.info("Select Private post option");
    }

    @When("Authenticated user create post")
    public void authenticated_user_create_post() throws InterruptedException {
        profilePage.createPost();
        Delay(1000);
        logger.info("Create post");
    }

    @Then("Post text should match")
    public void post_text_should_match() throws IOException {
        if (driver.findElement(profilePage.textPostPrivate).getText().equals("Selenium created post with private security!")) {
            assertEquals(driver.findElement(profilePage.textPostPrivate).getText(), "Selenium created post with private security!");
            logger.info("Assert post text - pass");
        } else {
            takeScreenShot(driver, "Assert post text - fail");
            assertEquals(driver.findElement(profilePage.textPostPrivate).getText(), "Selenium created post with private security!");
            logger.info("Assert post text - fail");
        }
    }

    @When("Non-friend user logs in")
    public void non_friend_user_logs_in() throws InterruptedException {
        Login("Rambo");
        logger.info("Non friend user logs in.");
    }

    @Then("Authenticated user can NOT see the private post of a stranger")
    public void authenticated_user_can_NOT_see_the_private_post_of_a_stranger() throws IOException {
        if (driver.findElement(profilePage.textPostPrivate).getText().equals("Selenium created post with private security!")) {
            assertNotEquals(driver.findElement(profilePage.textPostPrivate).getText(), "Selenium created post with private security!");
            logger.info("Asserting post visibility - pass");
        } else {
            takeScreenShot(driver, "Asserting post visibility - pass");
            assertNotEquals(driver.findElement(profilePage.textPostPrivate).getText(), "Selenium created post with private security!");
            logger.info("Asserting post visibility - pass");
        }
    }

    @Then("Authenticated user can see the private post of a friend")
    public void authenticated_user_can_see_the_private_post_of_a_friend() throws IOException {
        if (driver.findElement(profilePage.textPostPrivate).getText().equals("Selenium created post with private security!")) {
            assertEquals(driver.findElement(profilePage.textPostPrivate).getText(), "Selenium created post with private security!");
            logger.info("Asserting post visibility - pass");
        } else {
            takeScreenShot(driver, "Asserting post visibility - fail");
            assertEquals(driver.findElement(profilePage.textPostPrivate).getText(), "Selenium created post with private security!");
            logger.info("Asserting post visibility - fail");
        }
    }

    @And("Authenticated user deletes newly created post")
    public void authenticated_user_deletes_newly_created_post() throws InterruptedException {
        accountPage.deletePost();
        Delay(500);
        logger.info("Delete post");
    }

//Scenario: Registered user can like and unlike post

    @Then("likes counter of the post is {string}")
    public void likes_counter_of_the_post_is(String digit) throws IOException {
        if (driver.findElement(profilePage.likesCounter).getText().equals(digit)) {
            assertEquals(driver.findElement(profilePage.likesCounter).getText(), digit);
            logger.info("Assert likes counter - pass");
        } else {
            takeScreenShot(driver, "Assert likes counter - fail");
            assertEquals(driver.findElement(profilePage.likesCounter).getText(), digit);
            logger.info("Assert likes counter - fail");
        }
    }

    @When("Authenticated user likes post")
    public void authenticated_user_likes_post()throws InterruptedException {
        profilePage.LikePost();
        Delay(1000);
        logger.info("Like post");
    }

    @When("Authenticated user unlikes post")
    public void authenticated_user_unlikes_post()throws InterruptedException{
        profilePage.LikePost();
        Delay(1000);
        logger.info("Unlike post");
    }

    //Scenario: Registered user can comment post like and unlike comment

    @And("Authenticated user create comment for a post")
    public void authenticated_user_create_comment_for_a_post() throws InterruptedException{
        profilePage.createPost();
        Delay(1000);
        logger.info("Create comment");
    }

    @Then("Authenticated user can see the comment for the post")
    public void authenticated_user_can_see_the_comment_for_the_post() throws IOException {
        if (driver.findElement(profilePage.textComment).getText().equals("Selenium created comment.")) {
            assertEquals(driver.findElement(profilePage.textComment).getText(), "Selenium created comment.");
            logger.info("Asserting comment visibility - pass");
        } else {
            takeScreenShot(driver, "Asserting comment visibility - fail");
            assertEquals(driver.findElement(profilePage.textComment).getText(), "Selenium created comment.");
            logger.info("Asserting comment visibility - fail");
        }
    }

    @Then("likes counter of the comment is {string}")
    public void likes_counter_of_the_comment_is(String digit) throws IOException {
        if (driver.findElement(profilePage.likesCounterComments).getText().equals(digit)) {
            assertEquals(driver.findElement(profilePage.likesCounterComments).getText(), digit);
            logger.info("Asserting likes counter - pass");
        } else {
            takeScreenShot(driver, "Asserting likes counter - fail");
            assertEquals(driver.findElement(profilePage.likesCounterComments).getText(), digit);
            logger.info("Asserting likes counter - fail");
        }
    }

    @When("Authenticated user likes comment")
    public void authenticated_user_likes_comment()throws InterruptedException {
        profilePage.likeComment();
        Delay(1000);
        logger.info("Like comment");
    }

    @When("Authenticated user unlikes comment")
    public void authenticated_user_unlikes_comment()throws InterruptedException{
        profilePage.likeComment();
        Delay(1000);
        logger.info("Unlike comment");
    }

    //Scenario: Registered user can expand more comments

    @When("Authenticated user creates multiple comments: {int}")
    public void authenticated_user_creates_multiple_comments(Integer number) throws InterruptedException {
        profilePage.createMultipleComments(number);
        Delay(1000);
        logger.info("Creating multiple comments");
    }

    @When("refresh the page")
    public void refresh_the_page() throws InterruptedException {
        driver.navigate().refresh();
        Delay(1000);
    }

    @Then("only two comments should be visible")
    public void only_two_comments_should_be_visible() throws IOException {
        String divCommentsList = driver.findElement(profilePage.divCommentsList).getText();
        if (divCommentsList.contains("comment 1")) {
            assertFalse(divCommentsList.contains("comment 1"));
            logger.info("Asserting comments visibility - pass");
        } else {
            takeScreenShot(driver, "Asserting comments visibility - fail");
            assertFalse(divCommentsList.contains("comment 1"));
            logger.info("Asserting comments visibility - fail");
        }
    }

    @When("Authenticated user clicks on button Show more comments")
    public void authenticated_user_clicks_on_button_Show_more_comments() throws InterruptedException {
        profilePage.clickShowMoreComments();
        Delay(1000);
        ScrollPageDown();
        logger.info("Click button Show more comments");
    }

    @Then("all comments should be visible")
    public void all_comments_should_be_visible() throws IOException {
        String divCommentsListExpanded = driver.findElement(profilePage.divCommentsList).getText();
        if (divCommentsListExpanded.contains("comment 1")) {
            assertTrue(divCommentsListExpanded.contains("comment 1"));
            logger.info("Asserting comment visibility - pass");
        } else {
            takeScreenShot(driver, "Asserting comment visibility - fail");
            assertTrue(divCommentsListExpanded.contains("comment 1"));
            logger.info("Asserting comment visibility - fail");
        }
    }

    //Scenario: Post Feed Is Chronological

    @When("Authenticated user create post {string}")
    public void authenticated_user_create_post_with_string(String post) throws InterruptedException {
        profilePage.createPostMessage(post);
        Delay(1000);
        logger.info("Craete post");
    }

    @Then("Post text should match {string}")
    public void post_text_should_match(String post) throws IOException {
        if (driver.findElement(profilePage.textPostPrivate).getText().equals(post)) {
            assertEquals(driver.findElement(profilePage.textPostPrivate).getText(), post);
            logger.info("Asserting post text - pass");
        } else {
            takeScreenShot(driver, "Asserting post text - fail");
            assertEquals(driver.findElement(profilePage.textPostPrivate).getText(), post);
            logger.info("Asserting post text - fail");
        }
    }

    @Then("post {string} should be on the top of the feed")
    public void post_should_be_on_the_top_of_the_feed(String post) throws IOException {
        if (driver.findElement(accountPage.textPostAccount).getText().equals(post)) {
            assertEquals(driver.findElement(accountPage.textPostAccount).getText(), post);
            logger.info("Asserting post position - pass");
        } else {
            takeScreenShot(driver, "Asserting post position - fail");
            assertEquals(driver.findElement(accountPage.textPostAccount).getText(), post);
            logger.info("Asserting post position - fail");
        }
    }

    // Feature: Admin.feature ----------------------------------------------------------------------------------------

    //Scenario: Admin can edit user profile

    @When("Admin opens Admin Page")
    public void admin_opens_Admin_Page()throws InterruptedException {
        adminPage.openAdminPage();
        Delay(1000);
        logger.info("Admin opens to Admin Page.");
    }


    @When("Admin clicks on button Edit Profile")
    public void admin_clicks_on_button_Edit_Profile() throws InterruptedException{
        adminPage.clickAdminEditProfile();
        Delay(1000);
        logger.info("Admin clicks button Edit profile");
    }

    @When("Admin clicks on button Update")
    public void admin_clicks_on_button_Update()throws InterruptedException {
        adminPage.clickAdminUpdate();
        Delay(1000);
        logger.info("Admin clicks button Update");
    }

    //Scenario: Admin can edit and delete post

    @When("Admin edit a post with {string} message")
    public void admin_edit_a_post_with_message(String textPost) throws InterruptedException{
        adminPage.adminEditPost(textPost);
        logger.info("Admin editing post");
    }

    @Then("post text should be {string}")
    public void post_text_should_be(String textEditedPost) throws IOException {
        if (driver.findElement(adminPage.textPost).getText().equals(textEditedPost)) {
            assertEquals(driver.findElement(adminPage.textPost).getText(), textEditedPost);
            logger.info("Asserting post text - pass");
        }  else {
            takeScreenShot(driver,"Asserting post text - fail");
            assertEquals(driver.findElement(adminPage.textPost).getText(), textEditedPost);
            logger.info("Asserting post text - fail");
        }
    }

    @Then("Admin deletes post")
    public void admin_deletes_post() throws InterruptedException {
        adminPage.adminDeletePost();
        Delay(1000);
        logger.info("Admin deletes post");
    }

    //Scenario: Admin can edit and delete comment for a post

    @When("page is scrolled down")
    public void page_is_scrolled_down() throws InterruptedException {
        ScrollPageDown();
        logger.info("page is scrolled down");
    }

    @When("Admin clicks on button Edit comment")
    public void admin_clicks_on_button_Edit_comment() throws InterruptedException {
        driver.findElement(adminPage.buttonEditCommentAdmin).click();
        Delay(500);
        logger.info("Admin clicks button Edit comment");
    }

    @When("Admin edit comment with {string} and hit key Enter")
    public void admin_edit_comment_with(String newText) throws InterruptedException {
        adminPage.adminEditComment(newText);
        logger.info("Admin editing comment");
    }

    @Then("comment is edited with {string}")
    public void comment_is_edited_with(String textEdited) throws IOException {
        if (driver.findElement(adminPage.textEditedComment).getText().equals(textEdited)) {
            assertEquals(driver.findElement(adminPage.textEditedComment).getText(), textEdited);
            logger.info("Asserting comment text - pass");
        } else {
            takeScreenShot(driver, "comment text is not as expected");
            assertEquals(driver.findElement(adminPage.textEditedComment).getText(), textEdited);
            logger.info("Asserting comment text - fail");
        }
    }

    @Then("Admin clicks on button Delete comment")
    public void admin_clicks_on_button_Delete_comment() throws InterruptedException{
        adminPage.adminDeleteComment();
        Delay(1000);
        logger.info("Admin clicks on button Delete comment");
    }

    //Scenario: admin Can Delete User

    @When("Admin deletes User Profile")
    public void admin_deletes_User_Profile() {
        adminPage.deleteUser();
        logger.info("Admin deletes User profile");
    }


}
