package TestRunner;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {".//Features/"},
        glue = "StepDefinitions",
        dryRun = false,
        plugin = {"pretty","html:test-output"}
)

public class TestRunner {
}
