$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:Features/Admin.feature");
formatter.feature({
  "name": "Authenticated User",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "01 Admin can edit user profile",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"Arnold\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Admin opens Admin Page",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.admin_opens_Admin_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user search for profile \"Sherlock\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_search_for_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user clicks on Profile button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_clicks_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Admin clicks on button Edit Profile",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.admin_clicks_on_button_Edit_Profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Nationality United States",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Nationality_United_States()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Rank Legendary Eagle Master",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Rank_Legendary_Eagle_Master()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Map Inferno",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Map_Inferno()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Weapon M249",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Weapon_M()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User writes new description - \"Murdock...I am coming for you!\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_writes_new_description(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Admin clicks on button Update",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.admin_clicks_on_button_Update()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Nationality should be \"United States\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.nationality_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Rank should be \"Legendary Eagle Master\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.rank_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Map should be \"Inferno\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.map_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Weapon should be \"M249\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.weapon_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Description should be \"Murdock...I am coming for you!\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.description_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs in as \"Sherlock\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Account page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_clicks_on_Acount_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Nationality should be \"United States\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.nationality_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Rank should be \"Legendary Eagle Master\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.rank_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Map should be \"Inferno\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.map_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Weapon should be \"M249\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.weapon_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Description should be \"Murdock...I am coming for you!\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.description_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "02 Admin can edit and delete post",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post \"Captain Slow\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post_with_string(String)"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"#postValue\"}\n  (Session info: chrome\u003d78.0.3904.108)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027DESKTOP-4O1QSJO\u0027, ip: \u002710.188.69.244\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_231\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 78.0.3904.108, chrome: {chromedriverVersion: 78.0.3904.70 (edb9c9f3de024..., userDataDir: C:\\Users\\User\\AppData\\Local...}, goog:chromeOptions: {debuggerAddress: localhost:65206}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}\nSession ID: c0ceec37c1128b5cb38916d57d43fd6a\n*** Element info: {Using\u003did, value\u003dpostValue}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:372)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:188)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\r\n\tat PageObjects.ProfilePage.createPostMessage(ProfilePage.java:140)\r\n\tat StepDefinitions.Steps.authenticated_user_create_post_with_string(Steps.java:593)\r\n\tat ✽.Authenticated user create post \"Captain Slow\"(file:Features/Admin.feature:35)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs in as \"Arnold\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Admin opens Admin Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.admin_opens_Admin_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Admin edit a post with \"Admin edited post\" message",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.admin_edit_a_post_with_message(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "refresh the page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.refresh_the_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "post text should be \"Admin edited post\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_text_should_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Admin deletes post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.admin_deletes_post()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "03 Admin can edit and delete comment",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post \"Grand Tour is the new Top Gear\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post_with_string(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create comment for a post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_comment_for_a_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs in as \"Arnold\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Admin opens Admin Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.admin_opens_Admin_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "page is scrolled down",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.page_is_scrolled_down()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Admin clicks on button Edit comment",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.admin_clicks_on_button_Edit_comment()"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"div \u003e div:nth-child(5) \u003e button:nth-child(2) \u003e i\"}\n  (Session info: chrome\u003d78.0.3904.108)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027DESKTOP-4O1QSJO\u0027, ip: \u002710.188.69.244\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_231\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 78.0.3904.108, chrome: {chromedriverVersion: 78.0.3904.70 (edb9c9f3de024..., userDataDir: C:\\Users\\User\\AppData\\Local...}, goog:chromeOptions: {debuggerAddress: localhost:65246}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}\nSession ID: b8f45c45921a16e7bd8907183d01885d\n*** Element info: {Using\u003dcss selector, value\u003ddiv \u003e div:nth-child(5) \u003e button:nth-child(2) \u003e i}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByCssSelector(RemoteWebDriver.java:420)\r\n\tat org.openqa.selenium.By$ByCssSelector.findElement(By.java:431)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\r\n\tat StepDefinitions.Steps.admin_clicks_on_button_Edit_comment(Steps.java:685)\r\n\tat ✽.Admin clicks on button Edit comment(file:Features/Admin.feature:53)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "Admin edit comment with \"Fiat Panda 1.2i\" and hit key Enter",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.admin_edit_comment_with(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "comment is edited with \"Fiat Panda 1.2i\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.comment_is_edited_with(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Admin clicks on button Delete comment",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.admin_clicks_on_button_Delete_comment()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Admin deletes post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.admin_deletes_post()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "04 Admin can delete user",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"Arnold\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Admin opens Admin Page",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.admin_opens_Admin_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user search for profile \"James2\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_search_for_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user clicks on Profile button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_clicks_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Admin deletes User Profile",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.admin_deletes_User_Profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs in as \"James2\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:Features/AuthenticatedUser.feature");
formatter.feature({
  "name": "Authenticated User",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "01 Registered user can edit his profile",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"Rambo\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Account page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_clicks_on_Acount_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks button Edit profile",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_clicks_button_Edit_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Nationality United States",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Nationality_United_States()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Rank Legendary Eagle Master",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Rank_Legendary_Eagle_Master()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Map Inferno",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Map_Inferno()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select Weapon M249",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_select_Weapon_M()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User writes new description - \"Murdock...I am coming for you!\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_writes_new_description(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on button Update",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_clicks_on_button_Update()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Nationality should be \"United States\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.nationality_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Rank should be \"Legendary Eagle Master\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.rank_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Map should be \"Inferno\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.map_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Weapon should be \"M249\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.weapon_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Description should be \"Murdock...I am coming for you!\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.description_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "02 Registered users can connect and disconnect with each other",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"Rambo\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user search for profile \"Rocky Balboa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_search_for_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user clicks on Profile button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_clicks_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user sends friend request",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authencited_user_sends_friend_request()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs in as \"Rocky Balboa\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Friend request is displayed in Profile Page",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.friend_request_is_displayed_in_Profile_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user accept friend request",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_accept_friend_request()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "this new friend should appear in Friends list",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.this_new_friend_should_appear_in_Friends_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user clicks on button Unfriend",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_clicks_on_button_Unfriend()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "this user should not be a friend anymore",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.this_user_should_not_be_a_friend_anymore()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "03 Registered user can create public and private post",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user selects Private post option",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_selects_Private_post_option()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Post text should match",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_text_should_match()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Non-friend user logs in",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.non_friend_user_logs_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user can NOT see the private post of a stranger",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.authenticated_user_can_NOT_see_the_private_post_of_a_stranger()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Values should be different. Actual: Selenium created post with private security!\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failEquals(Assert.java:185)\r\n\tat org.junit.Assert.assertNotEquals(Assert.java:161)\r\n\tat org.junit.Assert.assertNotEquals(Assert.java:175)\r\n\tat StepDefinitions.Steps.authenticated_user_can_NOT_see_the_private_post_of_a_stranger(Steps.java:437)\r\n\tat ✽.Authenticated user can NOT see the private post of a stranger(file:Features/AuthenticatedUser.feature:44)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs in as \"Elon Musk\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Authenticated user can see the private post of a friend",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.authenticated_user_can_see_the_private_post_of_a_friend()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User clicks on Account page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_clicks_on_Acount_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Authenticated user deletes newly created post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_deletes_newly_created_post()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "04 Registered user can like and unlike post",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user selects Private post option",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_selects_Private_post_option()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Post text should match",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_text_should_match()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs in as \"Elon Musk\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user can see the private post of a friend",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.authenticated_user_can_see_the_private_post_of_a_friend()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "likes counter of the post is \"0\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.likes_counter_of_the_post_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user likes post",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_likes_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "likes counter of the post is \"1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.likes_counter_of_the_post_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user unlikes post",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_unlikes_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "likes counter of the post is \"0\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.likes_counter_of_the_post_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Account page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_clicks_on_Acount_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user deletes newly created post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_deletes_newly_created_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "05 Registered user can create a comment for a post, like and unlike comment",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user selects Private post option",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_selects_Private_post_option()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Post text should match",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_text_should_match()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create comment for a post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_comment_for_a_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs in as \"Elon Musk\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user can see the private post of a friend",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.authenticated_user_can_see_the_private_post_of_a_friend()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user can see the comment for the post",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.authenticated_user_can_see_the_comment_for_the_post()"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//div[contains(@id, \u0027comment-\u0027)]//p[contains(@id,\u0027text-\u0027)]\"}\n  (Session info: chrome\u003d78.0.3904.108)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027DESKTOP-4O1QSJO\u0027, ip: \u002710.188.69.244\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_231\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 78.0.3904.108, chrome: {chromedriverVersion: 78.0.3904.70 (edb9c9f3de024..., userDataDir: C:\\Users\\User\\AppData\\Local...}, goog:chromeOptions: {debuggerAddress: localhost:51606}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}\nSession ID: 74451086892869d653496f4fd7830e0e\n*** Element info: {Using\u003dxpath, value\u003d//div[contains(@id, \u0027comment-\u0027)]//p[contains(@id,\u0027text-\u0027)]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:428)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\r\n\tat StepDefinitions.Steps.authenticated_user_can_see_the_comment_for_the_post(Steps.java:504)\r\n\tat ✽.Authenticated user can see the comment for the post(file:Features/AuthenticatedUser.feature:81)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "likes counter of the comment is \"0\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.likes_counter_of_the_comment_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Authenticated user likes comment",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_likes_comment()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "likes counter of the comment is \"1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.likes_counter_of_the_comment_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Authenticated user unlikes comment",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_unlikes_comment()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "likes counter of the comment is \"0\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.likes_counter_of_the_comment_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User clicks on Account page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_clicks_on_Acount_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Authenticated user deletes newly created post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_deletes_newly_created_post()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "06 Registered user can expand more comments",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user selects Private post option",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_selects_Private_post_option()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Post text should match",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_text_should_match()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user creates multiple comments: 3",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_creates_multiple_comments(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "refresh the page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.refresh_the_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "only two comments should be visible",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.only_two_comments_should_be_visible()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user clicks on button Show more comments",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_clicks_on_button_Show_more_comments()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "all comments should be visible",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.all_comments_should_be_visible()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Account page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_clicks_on_Acount_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user deletes newly created post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_deletes_newly_created_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Home Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "07 Post feed is automatically sorted in chronological order",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User logs in as \"James\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_logs_in__as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user selects Private post option",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.authenticated_user_selects_Private_post_option()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post \"post 1\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post_with_string(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Post text should match \"post 1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_text_should_match(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user create post \"post 2\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_create_post_with_string(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Post text should match \"post 2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_text_should_match(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Account page",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_clicks_on_Acount_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "post \"post 2\" should be on the top of the feed",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_should_be_on_the_top_of_the_feed(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user deletes newly created post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_deletes_newly_created_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "post \"post 1\" should be on the top of the feed",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.post_should_be_on_the_top_of_the_feed(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Authenticated user deletes newly created post",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.authenticated_user_deletes_newly_created_post()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logs out",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.user_logs_out()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:Features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "Load Login Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Successful Login with valid credentials",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User enters Username as \"james\" and Password as \"password\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Email_as_and_Password_as(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"News feed page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Log out button",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_clicks_on_Log_out_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Counter-Strike Social\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Login DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User enters Username as \"\u003cname\u003e\" and Password as \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "click on Login button",
  "keyword": "And "
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "name",
        "password"
      ]
    },
    {
      "cells": [
        "Elon",
        "password"
      ]
    },
    {
      "cells": [
        "Elon Musk",
        "pass"
      ]
    },
    {
      "cells": [
        "",
        ""
      ]
    },
    {
      "cells": [
        "Elon Musk",
        ""
      ]
    },
    {
      "cells": [
        "",
        "password"
      ]
    }
  ]
});
formatter.background({
  "name": "Load Login Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User enters Username as \"Elon\" and Password as \"password\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Email_as_and_Password_as(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Login Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User enters Username as \"Elon Musk\" and Password as \"pass\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Email_as_and_Password_as(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Login Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User enters Username as \"\" and Password as \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Email_as_and_Password_as(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Login Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User enters Username as \"Elon Musk\" and Password as \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Email_as_and_Password_as(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Load Login Page",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User enters Username as \"\" and Password as \"password\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Email_as_and_Password_as(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:Features/Registration.feature");
formatter.feature({
  "name": "Registration",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.step({
  "name": "User enters Username as \"\u003cname\u003e\" and Password as \"\u003cpassword\u003e\" and email as \"\u003cemail\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "name",
        "password",
        "email"
      ]
    },
    {
      "cells": [
        "John",
        "password",
        ""
      ]
    },
    {
      "cells": [
        "",
        "",
        ""
      ]
    },
    {
      "cells": [
        "John",
        "",
        "john.connor@skynet.com"
      ]
    },
    {
      "cells": [
        "",
        "password",
        "john.connor@skynet.com"
      ]
    },
    {
      "cells": [
        "John",
        "",
        ""
      ]
    },
    {
      "cells": [
        "",
        "password",
        ""
      ]
    },
    {
      "cells": [
        "",
        "",
        "john.connor@skynet.com"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Registration_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters Username as \"John\" and Password as \"password\" and email as \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Username_as_and_Password_as_and_email_as(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Registration_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters Username as \"\" and Password as \"\" and email as \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Username_as_and_Password_as_and_email_as(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Registration_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters Username as \"John\" and Password as \"\" and email as \"john.connor@skynet.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Username_as_and_Password_as_and_email_as(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Registration_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters Username as \"\" and Password as \"password\" and email as \"john.connor@skynet.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Username_as_and_Password_as_and_email_as(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Registration_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters Username as \"John\" and Password as \"\" and email as \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Username_as_and_Password_as_and_email_as(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Registration_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters Username as \"\" and Password as \"password\" and email as \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Username_as_and_Password_as_and_email_as(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Registration DD with invalid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Registration page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Registration_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters Username as \"\" and Password as \"\" and email as \"john.connor@skynet.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_Username_as_and_Password_as_and_email_as(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Register button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Registration Page\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:Features/UnauthenticatedUser.feature");
formatter.feature({
  "name": "UnauthenticatedUser",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Unauthenticated user can navigate to HomePage",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "heading is Counter-Strike Social",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.i_verify_that_heading_is_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Unauthenticated user can NOT see profile data of registered users",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User open SocialNetwork HomePage",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_open_SocialNetwork_HomePage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User search for profile \"Elon Musk\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_search_for_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Profile button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.click_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Page title should be \"Login\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_title_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "navigate to Home Page",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.navigate_to_Home_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});