## I. Changed functionalities

-   **Public feed** – according to requirements the unauthenticated users should be able to see the public feed. But the DEV team has changed the requirements with permission by Upnetix. Now only registered users can see the public feed.
- **Public profiles** - according to requirements the unauthenticated users should be able to see the public profiles. But the DEV team has changed the requirements, with permission  by Upnetix. Now only registered users can see the public part of the profile

## II. Incomplete functionalities

- **Replies** – there are only endpoints for replies but they are not implemented in the UI.
