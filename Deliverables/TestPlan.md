﻿# TEST PLAN

> Version 1.0, 20.10.2019

> Prepared by: *Boris Stefanov* and *Ivaylo Yordanov*

### TABLE OF CONTENTS

1.  **[INTRODUCTION](#introduction)**
2.  **[OBJECTIVES AND TASKS](#objectives_and_task)**
3.  **[SCOPE](#scope)**
4.  **[FEATURES TO BE TESTED](#features_to_be_tested)**
5.  **[FEATURES NOT TO BE TESTED](#features_not_to_be_tested)**
6.  **[TESTING STRATEGY](#testing_strategy)**
7.  **[SCHEDULE AND TIMELINES](#schedule_and_timelines)**
8.  **[ENVIRONMENTAL REQUIREMENTS](#environmental_requirements)**
9.  **[TOOLS](#tools)**
10. **[ROLES AND RESPONSIBILITIES](#roles_and_responsibilities)**
11. **[RISKS AND ASSUMPTIONS](#risks_and_assumptions)**
12. **[CONTROL PROCEDURES](#control_procedures)**
13. **[REVISION HISTORY](#revision_history)**

### 1. <a name="introduction"></a>INTRODUCTION

[Counter-Strike](https://localhost:8080) (**CSGO**)
is a web-based application for communicating with fellow players.

**CSGO** offers opportunities for:
 - Connecting with people; 
 - Creating posts; 
 - Edit and delete post;
 - Like and unlike posts; 
 - Creating comments;
 - Edit and delete comments;
 - Like and unlike comments;
 - Creating replies
 - Like and unlike replies;
 - Edit and delete replies;
 - News feed of the newest posts; 
 - Private messages; 
 - User Profile Management; 
 - Admin Profile Management; 
 - Secure authentication and authorization; 
 - Search Engine;

#### Project Details

| | |
|-|-|  
| **Project Name** | Social Network
| **Product Name** | Counter-Strike: Global Offensive (CS:GO) |
| **Product Release Version** |  2.0 |
| **Product URL** | localhost:8080 |
  ----------------------------- ------------------------------------------

#### Definitions

| # | Abbreviation | Meaning |
|---|--------------|---------|
| 1. | CS:GO | Counter-Strike: Global Offensive |

### 2. <a name="objectives_and_task"></a>OBJECTIVES AND TASKS

#### 2.1. Objectives

This document is in accordance with the Service Level Agreement document
and it aims at:

-   outlining the overall testing effort needed for delivering fully
    operational and stable in-scope system features and their respective
    functionalities;
-   coordinating all necessary testing and control activities;
-   guaranteeing uninterrupted test workflow and adequate and flexible
    approach to eventual unexpected interruption / incident;
-   determining entry and exit criteria, time constraints, role
    responsibilities, means of communication, problem reporting, risk
    assessment and control activities;
-   facilitating communication between team members;
-   setting uniform understanding between team members of what is needed
    for successful completion of the testing tasks.

#### 2.2. Tasks

The main tasks that will be completed with the successful execution of
the test plan are:

-   Detailed analysis of the in-scope features and respective
    functionalities;
-   Authoring of high level test cases;
-   Manual test case execution;
-   Automating test cases;
-   Non-functional testing;
-   Preparation of detailed test and bug reports.

### 3. <a name="scope"></a>SCOPE

The testing efforts will be focused on sign up and sign in flow, post functionality, comment
functionality, connecting with users and user profile management features of
CS:GO.

### 4. <a name="features_to_be_tested"></a>FEATURES TO BE TESTED

All CS:GO features that fall into the testing scope of this document
will be reviewed and verified from a regular user’s and admin point of view:

-   Connect with users;
-   Disconnect with users;
-   Creation of posts;
-   Like and dislike posts;
-   Creation of commnets;
-   Like and dislike comments;
-   Get a feed of the newest/most relevant posts of your connections;
-   User Profile Management;
-   Login;
-   Registration;
-   Public profiles;
-   Private profiles;
-   Search profiles;
-   Admin features;

### 5. <a name="features_not_to_be_tested"></a>FEATURES NOT TO BE TESTED

- All optional features, unless they are implemented by the DEV team and their TRELLO status is in "Ready for test".

### 6. <a name="testing_strategy"></a>TESTING STRATEGY

#### 6.1. Testing levels

The testing effort of the QA team will be focused on System level where
end-to-end user scenarios will be tested via functional and regression
tests.

#### 6.2. Types of testing

-   **Unit Testing** - testing performed on each module or block of code
    during development. A unit is the smallest testable part of the
    software.

-   **Integration Testing** - level of software testing where individual
    units are combined and tested as a group. The purpose of this level
    of testing is to expose faults in the interaction between integrated
    units.

-   **Functional Testing** - testing conducted to evaluate the
    compliance of a component or system with functional requirements.

-   **Non-functional Testing** - check non-functional aspects -
    performance and load testing of the software application.

-   **Regression Testing** - testing of a previously tested component or
    system following modification to ensure that defects have not been
    introduced or have been uncovered in unchanged areas of the
    software, as a result of the changes made.

| Types of tests | Responsible team |
|----------------|------------------|
| Unit Tests | DEV Team |
| Integration Tests (API tests) | QA Team |
| Regression tests (smoke tests) | QA Team |
| Functional Testing (end-to-end scenarios) | QA Team |
| Non-functional Testing | QA Team |
| Acceptance Testing | Upnetix |

#### 6.2. Entry criteria

-   80% of unit and integration tests in DEV environment pass;
-   In-scope features are deployed in UAT environment and code freeze
    stage is met;
-   High level test cases are authored as per approved template;
-   All needed hardware and software is properly set up;
-   Quality testing data is set up;

#### 6.3. Exit criteria

-   100% of Prio1 tests pass;
-   90% of Prio2 tests pass;
-   100% of regression tests pass;
-   100% of bugs with Severity “Blocking” and “Critical” are fixed;
-   Time has ran out.

#### 6.4. Test Cases

#### 6.4.1. Template

All test cases should contain the following information:

-   Test Case ID
-   Title
-   Description
-   Pre-conditions
-   Steps to reproduce
-   Input data
-   Expected result
-   Priority

#### 6.4.2. Criteria for test case prioritization

Criteria that should be taken into consideration when setting test case
priority are:

-   Upnetix’s prioritization of requirements outlined in the Team
    Project Assignment document;
-   In-scope feature areas with highest number of previously logged bugs
    with high and medium priority;
-   In-scope feature areas of similar products which proved to be
    bottlenecks in the past;
-   In-scope feature functionalities which are considered most complex
    or with critical infrastructure;
-   In-scope feature functionalities with most visible to the client
    potential failures.

#### 6.4.3. Priority Levels

-   **Priority – 1 (Prio1)**: Allocated to all tests that must be
    executed in any case.
-   **Priority – 2 (Prio2)**: Allocated to the tests which can be
    executed, only when time permits.
-   **Priority – 3 (Prio3)**: Allocated to the tests, which even if not
    executed, will not cause big disruption.

#### 6.4.4. List with test cases

Full list with test cases can be found here -
https://counterstrike.testrail.io/index.php?/suites/overview/1

#### 6.5. Bug reports

All bug reports will be logged in GitLab Project “Social Media” -
https://gitlab.com/stan.ivanov93/social-media-project/issues

#### 6.5.1. Template

The bug reports should contain the following information: 
 - Title; 
 - Description; 
 - Steps to reproduce; 
 - Expected result; 
 - Actual result; 
 - Environment; 
 - Severity; 
 - Additional information (if applicable) - screenshots, images, videos, any other additional
   information which helps scope the issue.

#### 6.5.2. Bug life cycle

![Bug Life
Cycle](https://content.screencast.com/users/kocteh/folders/Default/media/21bfb169-26b5-45d4-84bf-b349fb132581/life_cycle_diagram.png)

#### 6.5.3. Bug severity levels

Severity is the degree of impact that a defect has on the development or
operation of a component or system. The severity will be set by
the person who finds and logs the bug.

There are four severity levels which will be used by the team:

-   *S1: Blocking* - a defect that completely hampers or blocks testing
    of the product / feature is a critical defect. It does not have a
    workaround.
-   *S2: Major* - a defect which needs immediate attention as it
    seriously hampers the feature usage / testing, however it has a
    workaround. The workaround is not obvious.
-   *S3: Minor* - the defect affects minor functionality or non-critical
    data. It has an easy workaround.
-   *S4: Trivial* - the defect does not affect functionality or data. It
    does not impact productivity or efficiency of the features.

<p style="text-align: center;">
    
<img src="https://content.screencast.com/users/kocteh/folders/Default/media/49d26c71-4104-4f68-8952-0618eb930000/bug_severity.png" alt="Bug Severity Levels"/>
</p>

As the severity will be used in the bug triage process and
prioritization, it is imperative that there is a common understanding
between the QA team members of what the term implies. When setting the
bug priority, it is important that the below key points are taken into
consideration:

-   Spend sufficient time obtaining clear understanding of how the
    defect will affect the end user;
-   Always assign the severity level based on the issue type as this
    will affect its priority.

#### 6.5.4. Bug prioritization and triaging

Priority is the level of (business) importance assigned to an item,
e.g., defect. The bug priority reveals the importance and the
urgency of the defect.

Bug prioritization will be performed during the triaging sessions. The
bug priority levels that will be used when reviewing bugs are:

-   *P1: High* - the bug needs immediate attention, it should be fixed
    right away and the features cannot be released with this bug.
-   *P2: Medium* - the bug needs to be fixed as early as possible after
    all critical bugs have been fixed.
-   *P3: Low* - the bug is with minor importance and its correction will
    be left for future releases, unless there are no bugs left with P1
    and P2.

Bug triaging will be performed at the beginning of every iteration by
the defect management committee which includes the Project Manager, Team
Leader team QA and team DEV. During triaging the defect management
committee will determine the priority of every bug and will select the
bugs that will be included in the iteration based on several
considerations:

-   Bug severity;
-   Issue type;
-   Impact;
-   Prioritization set by the client in the Service Level Agreement
    document.

Bug priority might change due to time constraints and changes in the
importance of the defect. In such cases the defect management committee
will re-prioritize the bug accordingly.

### 7. <a name="schedule_and_timelines"></a>SCHEDULE AND TIMELINES

Time for overall testing effort is set to 30 days. Time has
been allocated within the project plan for the following testing
activities:

| Task | Effort (in days) |
|------|-----------------|
| Test plan | 2 days |
| Low level test cases authoring | 2 days |
| High level test cases authoring | 2 days |
| Creating API tests | 2 days |
| Test Execution - manual | 2 days |
| Designing and implementing Test Automation Framework | 2 days |
| Test cases automation | 3 days |
| Set up CI | 1 day |

### 8. <a name="environmental_requirements"></a>ENVIRONMENTAL REQUIREMENTS

#### 8.1. Hardware requirements:

-   Two laptops on Windows and Linux;

#### 8.2. Software requirements:

-   Latest versions of Google Chrome, Mozilla Firefox on Windows 10, Ubuntu;

### 9. <a name="tools"></a>TOOLS

-   Bug tracking and reporting system - GitLab
    https://gitlab.com/stan.ivanov93/social-media-project/issues
-   Test case tracking system - TestRail
    https://socialnetwork.testrail.io/index.php?/projects/overview/1
-   Postman
-   Rest Assured + Java + TestNG
-   Selenium WebDriver + Maven + TestNG
-   Selenium WebDriver + Cucumber + JUnit
-   Apache JMeter

### 10. <a name="roles_and_responsibilities"></a>ROLES AND RESPONSIBILITIES

| Name | Role |
|------|------|
| Ivaylo Yordanov | QA team |
| Boris Stefanov | QA team |
| Pavlina Koleva | PM(PO) |
| Petya Grozdarska | Scrum Master |
| Upnetix | Customer |

### 11. <a name="risks_and_assumptions"></a>RISKS AND ASSUMPTIONS

-   code freeze stage was not reached on time / development of in-scope
    features is not finished on time;
-   test effort timelines were unrealistically set;
-   unexpected unavailability of team members both from the developers
    and from the QAs;
-   lack of availability of required hardware, software, data or tools;
-   late delivery of the software, hardware or tools;
-   changes to the original requirements or designs;
-   changes in the prioritization of requirements set by the client in
    the Service Level Agreement document;
-   unexpected complexities in testing of the in-scope features.

### 12. <a name="control_procedures"></a>CONTROL PROCEDURES

The current test plan has been created to improve the productivity of
the testing effort. Consequently, it is essential that correct
procedures are set in place to ensure that deviations from the test plan
can be addressed in a timely manner and in the best possible way.
Control helps us deal with any unexpected situation that might impede
the testing activities needed for the delivery of a quality in-scope
features.

All control activities will be performed by the Project Manager. The
Project Manager will ensure that:

-   all team members involved in the quality assurance process
    understand correctly and agree with the activities described in this
    document at any given step;
-   all testing activities follow the rules set in the document;
-   all testing activities are on track and performed on time;
-   all misunderstandings are cleared out in a timely manner;
-   all deviations from the processes described in this document are
    found on time and are addressed in a timely manner;
-   there are set procedures which help the team answer any unexpected
    situation which might impede or delay the testing activities;
-   all team members are familiar with the ways an encountered incident
    can be reported;
-   all control activities are conducted in a discrete way which does
    not interrupt in any way the normal workflow and day-to-day work of
    the team members.

#### Reporting incidents:

Every Saturday during the team brief meetings on the progress of the
testing work, 20 minutes will be used for incident reporting and
incident resolution progress reporting so that the whole team is aware
of any obstacles that were encountered and the impact that they (might)
have on the progress of the testing effort. If, for any reason, the
incident reports cannot be performed during these meetings, any team
member may send an email to the Project Manager and Team Leader
explaining as much as possible the circumstances of the incident (when
it occurred, what is the potential impact, what part of the testing
process is affected, any piece of information that might assist in
finding the root cause).

#### Addressing incidents:

If the incident affects any main point outlined in this document (e.g.
estimated timelines and resources, outlined environmental needs, entry
and exit criteria) and its resolution cannot be immediately reached, the
Project Manager will ensure that the agreed changes are reflected in the
correct document section and they are recorded in the document revision
history.

In case of absence of the Project Manager, the Team Leader will carry on
with the PM’s control responsibilities.

### 13. <a name="revision_history"></a>REVISION HISTORY

| Type of Change | Name of contributor(s) | Date of Creation | Approved By | Date of Approval |
|--------------------|------------------------|------------------|-------------|------------------|
| Initial Draft | Boris Stefanov and Ivaylo Yordanov | 23.10.2019 |                                                              


