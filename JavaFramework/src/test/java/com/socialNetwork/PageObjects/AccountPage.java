package com.socialNetwork.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AccountPage {

    WebDriver ldriver;

    public AccountPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[2]/button[3]")
    WebElement buttonEditProfile;

    @FindBy(id = "nationalityValue")
    WebElement ddNationality;

    @FindBy(id = "rankValue")
    WebElement ddRank;

    @FindBy(id = "mapValue")
    WebElement ddMap;

    @FindBy(id = "weaponValue")
    WebElement ddWeapon;

    @FindBy(id = "descriptionValue")
    WebElement fieldDescription;

    @FindBy(xpath = "//*[@id=\"Change profile settings\"]/div/div/div/div/div/div[13]/button")
    public WebElement buttonUpdate;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[1]/div/div[1]/p/span[3]")
    public WebElement textRank;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[1]/div/div[2]/p/span[3]")
    public WebElement textNationlity;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[1]/div/div[3]/p/span[3]")
    public WebElement textWeapon;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[1]/div/div[4]/p/span[3]")
    public WebElement textMap;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[1]/div/div[5]/p[2]")
    public WebElement textDescription;

    @FindBy(xpath = "//*[@id=\"post-header\"]/div/div[4]/button[1]")
    WebElement buttonDeletePost;

    @FindBy(xpath = "//div[contains(@id,'postDiv')]//p[contains(@id,'text-')]")
    public WebElement textPostAccount;

    public void clickButtonEditProfile(){
        buttonEditProfile.click();
    }

    public void selectNationality(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ddNationality).perform();
        Select select = new Select(ddNationality);
        select.selectByVisibleText("United States");
    }

    public void selectRank(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ddRank).perform();
        Select select = new Select(ddRank);
        select.selectByVisibleText("Legendary Eagle Master");
    }

    public void selectMap(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ddMap).perform();
        Select select = new Select(ddMap);
        select.selectByVisibleText("Inferno");
    }

    public void selectWeapon(){
        Actions action = new Actions(ldriver);
        action.moveToElement(ddWeapon).perform();
        Select select = new Select(ddWeapon);
        select.selectByVisibleText("M249");
    }

    public void editDescription(){
        fieldDescription.click();
        fieldDescription.clear();
        fieldDescription.sendKeys("Murdock...I am coming for you!");
    }

    public void clickUpdate(){
        Actions action = new Actions(ldriver);
        action.moveToElement(buttonUpdate).perform();
        buttonUpdate.click();
    }

    public void deletePost(){
        buttonDeletePost.click();
    }



}
