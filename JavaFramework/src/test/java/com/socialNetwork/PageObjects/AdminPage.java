package com.socialNetwork.PageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {

    WebDriver ldriver;

    public AdminPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    @FindBy(xpath = "/html/body/div[1]/div/a[4]")
    WebElement buttonAdminPage;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[2]/button[2]")
    WebElement buttonAdminEditProfile;

    @FindBy(xpath = "//*[@id=\"Sherlock\"]/i")
    WebElement buttonAdminUpdate;

    @FindBy(xpath = "//button[contains(@onClick,'')]//i[contains(@class,'fas fa-screwdriver')]")
    WebElement buttonEditPost;

    @FindBy(id = "//div[contains(@id,'post-content')]//input")
    public WebElement fieldPost;

    @FindBy(xpath = "//button[contains(@class,'w3-margin-left')]//i[contains(@class,'fas fa-trash-alt')]")
    WebElement buttonDeletePost;

    @FindBy(id = "6")
    WebElement buttondeleteUser;

    @FindBy(xpath = "//div[contains(@id,'postDiv')]//p")
    public WebElement textPost;


    public void openAdminPage(){
        buttonAdminPage.click();
    }

    public void clickAdminEditProfile(){
        buttonAdminEditProfile.click();
    }

    public void clickAdminUpdate(){
        buttonAdminUpdate.click();
    }

    public void adminEditPost() throws InterruptedException{
        buttonEditPost.click();
        Thread.sleep(1000);
        fieldPost.click();
        Thread.sleep(500);
        fieldPost.clear();
        Thread.sleep(500);
        fieldPost.sendKeys("Admin edited post");
        Thread.sleep(500);
        fieldPost.sendKeys(Keys.ENTER);
        Thread.sleep(1000);
    }

    public void adminDeletePost(){
        buttonDeletePost.click();
    }

    public  void deleteUser(){
        buttondeleteUser.click();
    }


}
