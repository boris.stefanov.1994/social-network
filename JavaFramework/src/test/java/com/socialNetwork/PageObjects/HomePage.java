package com.socialNetwork.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver ldriver;

    public HomePage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    @FindBy(xpath = "//*[@id=\"navbarResponsive\"]/ul/li[2]/a")
    @CacheLookup
    WebElement buttonHomeLogin;

    @FindBy(xpath = "//*[@id=\"navbarResponsive\"]/ul/li[1]/a")
    @CacheLookup
    WebElement buttonHomeRegister;

    @FindBy(id = "input")
    WebElement fieldSearch;

    @FindBy(xpath = "//*[@id=\"users-div\"]/div/div/div[3]/a/button")
    WebElement buttonProfile;

    public void clickHomeLogin(){
        buttonHomeLogin.click();
    }

    public void clickHomeRegister(){
        buttonHomeRegister.click();
    }

    public void search(){
        fieldSearch.click();
        fieldSearch.sendKeys("Elon Musk");
    }

    public void clickProfileButton(){
        buttonProfile.click();
    }

}
