package com.socialNetwork.PageObjects;

import com.socialNetwork.TestCases.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.logging.Logger;

public class LoginPage {

    WebDriver ldriver;

    public LoginPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    @FindBy(id="username")
    @CacheLookup
    WebElement fieldUsername;

    @FindBy(id="password")
    @CacheLookup
    WebElement fieldPassword;

    @FindBy(xpath="//*[@id=\"regTable\"]/div/div/div/div/form/input")
    @CacheLookup
    WebElement buttonLogin;


    public void enterUsername(String uname){
        fieldUsername.sendKeys(uname);
    }

    public void enterPassword(String pwd){
        fieldPassword.sendKeys(pwd);
    }

    public void clickLogin(){
        buttonLogin.click();
    }





}
