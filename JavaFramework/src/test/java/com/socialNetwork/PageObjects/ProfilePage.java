package com.socialNetwork.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class ProfilePage {

    WebDriver ldriver;

    public ProfilePage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    @FindBy(xpath = "/html/body/div[1]/div/a[3]")
    @CacheLookup
    WebElement buttonLogout;

    @FindBy(xpath = "/html/body/div[1]/div/a[2]")
    WebElement buttonAcount;

    @FindBy(id = "input")
    WebElement fieldSearchProfile;

    @FindBy(xpath = "//*[@id=\"users-div\"]/div/div[3]/a/button")
    WebElement buttonProfile;

    @FindBy(xpath = "/html/body/div[3]/div/div[3]/div[2]/div/div/div[1]/h4")
    public WebElement divFriendRequest;

    @FindBy(xpath = "/html/body/div[3]/div/div[3]/div[2]/div/div/div[2]/div[2]/h5")
    public WebElement textFriendRequest;

    @FindBy(xpath = "//*[@id=\"3\"]")
    WebElement buttonAccept;

    @FindBy(xpath = "//*[@id=\"friendsDiv\"]/div/div[2]/a/h4")
    public WebElement textFriend;

    @FindBy(xpath = "//*[@id=\"3\"]")
    WebElement buttonUnfriend;

    @FindBy(xpath = "//*[@id=\"friendsDiv\"]/div/p")
    public WebElement textNoFriends;

    @FindBy(id = "postSecure")
    WebElement ddPostSecurity;

    @FindBy(id = "postValue")
    WebElement fieldPost;

    @FindBy(xpath = "//*[@id=\"post-content\"]/p[1]")
    public WebElement textPostPrivate;

    @FindBy(xpath = "/html/body/div[3]/div/div[2]/div[1]/div/div/div[2]/div/div/form/button")
    WebElement buttonPost;

    @FindBy(xpath = "//*[@id=\"post-content\"]/button[1]")
    WebElement butonLikePost;

    @FindBy(id = "likes-count")
    public WebElement likesCounter;

    @FindBy(id = "commentValue")
    WebElement fieldComment;

    @FindBy(xpath = "//div[contains(@id, 'post-')]//button[contains(@onclick, 'writeComment')]")
    WebElement buttonComment;

    @FindBy(xpath = "//div[contains(@id, 'comment-')]//p[contains(@id,'text-')]")
    public WebElement textComment;

    @FindBy(xpath = "//div[contains(@id, 'comment-')]//button")
    WebElement buttonLikeComment;

    @FindBy(id = "comment-likes-count")
    public WebElement likesCounterComments;

    @FindBy(xpath = "//div[contains(@id,'post-')]//button[contains(@id,'comment-')]")
    WebElement buttonShowMoreComments;


    public void clickLogout(){
        buttonLogout.click();
    }

    public void clickAccountPage(){
        buttonAcount.click();
    }

    public void searchProfile(String name){
        fieldSearchProfile.click();
        fieldSearchProfile.sendKeys(name);
    }

    public void clickButtonUsernameProfile(){
        buttonProfile.click();
    }

    public void clickAccept(){
        buttonAccept.click();
    }

    public void clickUnfriend(){
        buttonUnfriend.click();
    }

    public void selectPrivatePost(){
        Select select = new Select(ddPostSecurity);
        select.selectByVisibleText("Private");
    }

    public void createPost() throws InterruptedException{
        fieldPost.clear();
        fieldPost.sendKeys("Selenium created post with private security!");
        Thread.sleep(1000);
        buttonPost.click();
    }

    public void LikePost(){
        butonLikePost.click();
    }

    public void createComment() throws InterruptedException {
        fieldComment.click();
        fieldComment.clear();
        fieldComment.sendKeys("Selenium created comment.");
        Thread.sleep(1000);
        buttonComment.click();
        Thread.sleep(1000);
    }

    public void likeComment(){
        buttonLikeComment.click();
    }

    public void createMultipleComments(int iterations) throws InterruptedException {
        for (int i = 1; i <= iterations; i++) {
            fieldComment.clear();
            fieldComment.sendKeys("comment " + i);
            Thread.sleep(1500);
            buttonComment.click();
            Thread.sleep(500);
        }
    }

    public void clickShowMoreComments(){
        buttonShowMoreComments.click();
    }

    public void createPostMessage(String message) throws InterruptedException{
        fieldPost.clear();
        fieldPost.sendKeys(message);
        Thread.sleep(1000);
        buttonPost.click();
    }

}
