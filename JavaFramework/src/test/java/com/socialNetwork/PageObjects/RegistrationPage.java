package com.socialNetwork.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {

    WebDriver ldriver;

    public RegistrationPage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);
    }

    @FindBy(id="username")
    @CacheLookup
    WebElement fieldUsername;

    @FindBy(id="password")
    @CacheLookup
    WebElement fieldPassword;

    @FindBy(id="email")
    @CacheLookup
    WebElement fieldEmail;

    @FindBy(xpath = "//*[@id=\"regTable\"]/div/div/div/div/form/input")
    @CacheLookup
    WebElement buttonRegister;

    @FindBy(xpath = "/html/body/h2")
    public WebElement textSuccessfulRegistration;

    public void enterUsername(String uname){
        fieldUsername.sendKeys(uname);
    }

    public void enterPassword(String pwd){
        fieldPassword.sendKeys(pwd);
    }

    public void enterEmail(String email){
        fieldEmail.sendKeys(email);
    }

    public void clickRegister(){
        buttonRegister.click();
    }

    public String getMessageSuccessfulRegistration(){
        return textSuccessfulRegistration.getText();
    }



}
