package com.socialNetwork.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserProfilePage {

    WebDriver ldriver;

    public UserProfilePage(WebDriver rdriver){
        ldriver = rdriver;
        PageFactory.initElements(rdriver, this);

    }

    @FindBy(id = "4")
    public WebElement buttonSendFriendRequest;

    public void clickButtonSendConnection(){
        buttonSendFriendRequest.click();
    }

}
