package com.socialNetwork.TestCases;

import com.beust.jcommander.Parameter;
import com.socialNetwork.Utilities.ReadConfig;
import com.socialNetwork.Utilities.XLUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseClass {

    ReadConfig readConfig = new ReadConfig();

    public String baseURL = readConfig.getApplicationURL();
    public String loginURL = readConfig.goToLoginPage();
    public String logoutXpath = readConfig.getLogoutXpath();
    public String password = readConfig.getPassword();
    public String buttonHomeLogin = readConfig.getXpathButtonHomeLogin();
    public String buttonLogin = readConfig.getXpathButtonLogin();

    public static WebDriver driver;
    public WebDriverWait wait;
    public static Logger logger;
    //public SoftAssert softAssert; //Don't forget to put softAssert.assertAll() at the end of the test!


    @Parameters("browser")
    @BeforeClass
    public void SetUp(String browser){

        logger = Logger.getLogger("social network");
        PropertyConfigurator.configure("Log4j.properties");
        //softAssert = new SoftAssert();

        if (browser.equals("chrome")){
            System.setProperty("webdriver.chrome.driver", readConfig.getChromePath());
            driver = new ChromeDriver();
        } else if (browser.equals("firefox")){
            System.setProperty("webdriver.gecko.driver", readConfig.getFireFoxPath());
            driver = new FirefoxDriver();
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseURL);

    }

    @AfterClass
    public void TearDown(){
        //softAssert.assertAll();
        driver.quit();
    }

    public void takeScreenShot(WebDriver driver, String tname) throws IOException {

        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        File target = new File(System.getProperty("user.dir") + "/Screenshots/" + tname + ".png");
        FileUtils.copyFile(source, target);
        System.out.println("Screenshot taken");
    }

    @DataProvider(name = "LoginData")
    public Object[][] getLoginData() throws IOException {
        String path = System.getProperty("user.dir") + "/src/test/java/com/socialNetwork/TestData/LoginData.xlsx";
        int rownum = XLUtils.getRowCount(path, "Sheet1");
        int colcount = XLUtils.getCellCount(path, "Sheet1", 1);

        String[][] loginData = new String[rownum][colcount];

        for (int i = 1; i <= rownum ; i++) {
            for (int j = 0; j < colcount; j++) {
                loginData[i-1][j] = XLUtils.getCellData(path, "Sheet1", i,j);
            }
        }
        return loginData;
    }

    @DataProvider(name = "RegistrationData")
    public Object[][] getRegistrationData() throws IOException {
        String path = System.getProperty("user.dir") + "/src/test/java/com/socialNetwork/TestData/RegistrationData.xlsx";
        int rownum = XLUtils.getRowCount(path, "Sheet1");
        int colcount = XLUtils.getCellCount(path, "Sheet1", 1);

        String[][] loginData = new String[rownum][colcount];

        for (int i = 1; i <= rownum ; i++) {
            for (int j = 0; j < colcount; j++) {
                loginData[i-1][j] = XLUtils.getCellData(path, "Sheet1", i,j);
            }
        }
        return loginData;
    }

    public void Login(String name) throws InterruptedException {
        Delay(1000);
        driver.findElement(By.xpath(buttonHomeLogin)).click();
        Delay(1000);
        driver.findElement(By.id("username")).sendKeys(name);
        Delay(1000);
        driver.findElement(By.id("password")).sendKeys(password);
        Delay(1000);
        driver.findElement(By.xpath(buttonLogin)).click();
        Delay(1000);
    }

    public void Delay(int miliseconds)throws InterruptedException{
        Thread.sleep(miliseconds);
    }

    public void ScrollPageDown()throws InterruptedException{
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,350)");
        Delay(500);
    }

}
