package com.socialNetwork.TestCases;

import com.socialNetwork.PageObjects.AccountPage;
import com.socialNetwork.PageObjects.AdminPage;
import com.socialNetwork.PageObjects.ProfilePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;

public class TC_Admin extends BaseClass {

    @Test
    public void adminCanEditUserProfile() throws InterruptedException {

        logger.info("Executing TC_adminCanEditUserProfile.");
        // Arrange
        AdminPage adminPage = new AdminPage(driver);
        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        SoftAssert softAssert = new SoftAssert();

        // Act
        Login("Arnold");
        adminPage.openAdminPage();
        Delay(1000);
        profilePage.searchProfile("Sherlock");
        Delay(1000);
        profilePage.clickButtonUsernameProfile();
        Delay(1000);
        adminPage.clickAdminEditProfile();
        Delay(1000);
        accountPage.selectNationality();
        Delay(1000);
        accountPage.selectRank();
        Delay(1000);
        accountPage.selectMap();
        Delay(1000);
        accountPage.selectWeapon();
        Delay(1000);
        accountPage.editDescription();
        Delay(1000);
        adminPage.clickAdminUpdate();
        Delay(1000);

        // Assert
        softAssert.assertEquals(accountPage.textRank.getText(),"Legendary Eagle Master");
        softAssert.assertEquals(accountPage.textNationlity.getText(),"United States");
        softAssert.assertEquals(accountPage.textWeapon.getText(), "M249");
        softAssert.assertEquals(accountPage.textMap.getText(),"Inferno");
        softAssert.assertEquals(accountPage.textDescription.getText(),"Murdock...I am coming for you!");

        profilePage.clickLogout();

        Login("Sherlock");
        profilePage.clickAccountPage();
        Delay(1000);
        softAssert.assertEquals(accountPage.textRank.getText(),"Legendary Eagle Master");
        softAssert.assertEquals(accountPage.textNationlity.getText(),"United States");
        softAssert.assertEquals(accountPage.textWeapon.getText(), "M249");
        softAssert.assertEquals(accountPage.textMap.getText(),"Inferno");
        softAssert.assertEquals(accountPage.textDescription.getText(),"Murdock...I am coming for you!");

        driver.findElement(By.xpath(logoutXpath)).click();
        softAssert.assertAll();
        logger.info("Finished TC_adminCanEditUserProfile.");

    }

    @Test
    public void adminCanEditAndDeletePost() throws InterruptedException {

        logger.info("Executing TC_adminCanEditAndDeletePost.");

        // Arrange
        AdminPage adminPage = new AdminPage(driver);
        ProfilePage profilePage = new ProfilePage(driver);
        SoftAssert softAssert = new SoftAssert();

        //Act
        Login("James");
        profilePage.createPostMessage("Captain Slow");
        Delay(1000);
        driver.findElement(By.xpath(logoutXpath)).click();
        Login("Arnold");
        adminPage.openAdminPage();
        Delay(1000);
        //adminPage.adminEditPost();
        driver.findElement(By.xpath("//button[contains(@onClick,'')]//i[contains(@class,'fas fa-screwdriver')]")).click();
        Thread.sleep(1000);
        WebElement postField = driver.findElement(By.xpath("//div[contains(@id,'post-content')]//input"));
        postField.click();
        Thread.sleep(500);
        postField.clear();
        Thread.sleep(500);
        postField.sendKeys("Admin edited post");
        Thread.sleep(500);
        postField.sendKeys(Keys.ENTER);
        Thread.sleep(1000);

        //Assert
        driver.navigate().refresh();
        Delay(500);
        softAssert.assertEquals(adminPage.textPost.getText(), "Admin edited post");
        Delay(500);
        adminPage.adminDeletePost();
        Delay(1000);
        driver.findElement(By.xpath(logoutXpath)).click();

        softAssert.assertAll();
        logger.info("Finished TC_adminCanEditAndDeletePost.");

    }

    @Test
    public void adminCanEditAndDeleteComment() throws InterruptedException{

        logger.info("Executing TC_adminCanEditAndDeleteComment.");

        // Arrange
        AdminPage adminPage = new AdminPage(driver);
        ProfilePage profilePage = new ProfilePage(driver);
        SoftAssert softAssert = new SoftAssert();

        //Act
        Login("James");
        profilePage.createPostMessage("Captain Slow");
        Delay(1000);
        profilePage.createComment();
        driver.findElement(By.xpath(logoutXpath)).click();

        Login("Arnold");
        adminPage.openAdminPage();
        Delay(1000);
        ScrollPageDown();
        Delay(3000);
        driver.findElement(By.cssSelector("div > div:nth-child(5) > button:nth-child(2) > i")).click();
        Delay(500);
        WebElement fieldComment = driver.findElement(By.xpath("//div[contains(@class,' w3-round')]//input[contains(@id,'textComment')]"));
        fieldComment.click();
        fieldComment.clear();
        Delay(500);
        fieldComment.sendKeys("Fiat Panda 1.2i");
        Delay(1000);
        fieldComment.sendKeys(Keys.ENTER);
        Delay(1000);
        WebElement textComment = driver.findElement(By.xpath("//div[contains(@id,'comment-')]//div[contains(@class,'w3-round')]//p"));
        Delay(500);

        softAssert.assertEquals(textComment.getText(), "Fiat Panda 1.2i");
        driver.findElement(By.xpath("//div[contains(@id,'comments-list')]//button[contains(@class,'w3-margin-left')]//i[contains(@class,'fas fa-trash-alt')]")).click();
        Delay(500);
        adminPage.adminDeletePost();
        Delay(1000);
        driver.findElement(By.xpath(logoutXpath)).click();

        softAssert.assertAll();
        logger.info("Finished TC_adminCanEditAndDeleteComment.");

    }

    @Test(enabled = false)
    public  void adminCanDeleteUser() throws InterruptedException, IOException {

        logger.info("Executing TC_adminCanDeleteUser.");

        AdminPage adminPage = new AdminPage(driver);
        ProfilePage profilePage = new ProfilePage(driver);
        SoftAssert softAssert = new SoftAssert();

        Login("Arnold");
        adminPage.openAdminPage();
        Delay(1000);
        profilePage.searchProfile("James2");
        Delay(1000);
        profilePage.clickButtonUsernameProfile();
        Delay(1000);
        adminPage.deleteUser();
        Delay(1000);
        driver.findElement(By.xpath(logoutXpath)).click();

        Login("James2");

        //Assert
        if (driver.getTitle().equals("Login")) {
            softAssert.assertEquals(driver.getTitle(), "Login");
            logger.info("Finished TC adminCanDeleteUser. User is deleted - TC PASS!");
        } else {
            softAssert.assertEquals(driver.getTitle(), "News feed page");
            logger.info("Finished TC adminCanDeleteUser. User is NOT deleted - TC FAIL!");
            takeScreenShot(driver, "adminCanDeleteUser test");
        }

        driver.navigate().to(baseURL);
        softAssert.assertAll();
    }


}