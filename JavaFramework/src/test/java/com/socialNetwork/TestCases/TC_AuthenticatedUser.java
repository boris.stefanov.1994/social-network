package com.socialNetwork.TestCases;

import com.socialNetwork.PageObjects.AccountPage;
import com.socialNetwork.PageObjects.HomePage;
import com.socialNetwork.PageObjects.ProfilePage;
import com.socialNetwork.PageObjects.UserProfilePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.text.DecimalFormat;
import java.util.List;

public class TC_AuthenticatedUser extends BaseClass {

    @Test
    public void registeredUserCanEditHisProfile() throws InterruptedException {

        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        SoftAssert softAssert = new SoftAssert();

        logger.info("Executing TC registeredUserCanEditHisProfile.");
        Login("Rambo");
        profilePage.clickAccountPage();
        accountPage.clickButtonEditProfile();
        Delay(1000);
        accountPage.selectNationality();
        Delay(1000);
        accountPage.selectRank();
        Delay(1000);
        accountPage.selectMap();
        Delay(1000);
        accountPage.selectWeapon();
        Delay(1000);
        accountPage.editDescription();
        Delay(1000);
        accountPage.clickUpdate();
        Delay(1000);

        softAssert.assertEquals(accountPage.textRank.getText(), "Legendary Eagle Master");
        softAssert.assertEquals(accountPage.textNationlity.getText(), "United States");
        softAssert.assertEquals(accountPage.textWeapon.getText(), "M249");
        softAssert.assertEquals(accountPage.textMap.getText(), "Inferno");
        softAssert.assertEquals(accountPage.textDescription.getText(), "Murdock...I am coming for you!");

        //softAssert.assertAll();

        profilePage.clickLogout();
        logger.info("Finished TC registeredUserCanEditHisProfile.");
        Delay(1500);
        softAssert.assertAll();

    }

    @Test
    public void registeredUsersCanConnectAndDisconnect() throws InterruptedException {

        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        UserProfilePage userProfilePage = new UserProfilePage(driver);
        SoftAssert softAssert = new SoftAssert();

        logger.info("Executing TC registeredUsersCanConnectAndDisconnect.");
        Login("Rambo");
        profilePage.searchProfile("Rocky Balboa");
        Delay(1000);
        profilePage.clickButtonUsernameProfile();
        Delay(1000);
        userProfilePage.clickButtonSendConnection();
        Delay(1000);
        profilePage.clickLogout();
        Delay(1000);
        Login("Rocky Balboa");
        Delay(1000);
        softAssert.assertTrue(profilePage.divFriendRequest.isDisplayed());
        softAssert.assertEquals(profilePage.textFriendRequest.getText(), "Rambo");
        profilePage.clickAccept();
        Delay(1000);
        softAssert.assertEquals(profilePage.textFriend.getText(), "Rambo");
        profilePage.clickUnfriend();
        Delay(1000);
        softAssert.assertTrue(profilePage.textNoFriends.isDisplayed());
        softAssert.assertEquals(profilePage.textNoFriends.getText(), "You still don't have any buddies");

        logger.info("Finished TC registeredUsersCanConnectAndDisconnect.");
        driver.findElement(By.xpath(logoutXpath)).click();
        Delay(1500);
        softAssert.assertAll();

    }

    @Test
    public void registeredUserCanCreatePublicAndPrivatePost() throws InterruptedException {

        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        SoftAssert softAssert = new SoftAssert();

        logger.info("Executing TC registeredUserCanCreatePublicAndPrivateComment.");

        Login("James");
        Delay(1000);
        profilePage.selectPrivatePost();
        Delay(500);
        profilePage.createPost();
        Delay(1000);
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");
        driver.findElement(By.xpath(logoutXpath)).click();
        Delay(1000);
        //Pre-requisite: Rambo and James May are NOT friends!
        Login("Rambo");
        softAssert.assertNotEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");
        driver.findElement(By.xpath(logoutXpath)).click();
        Delay(1000);
        //Pre-requisite: Elon Musk and James May are friends!
        Login("Elon Musk");
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");
        driver.findElement(By.xpath(logoutXpath)).click();
        Delay(1000);
        Login("James");
        //accountPage.clickButtonEditProfile();
        driver.findElement(By.xpath("/html/body/div[1]/div/a[2]")).click();
        Delay(1000);
        //Deletes the newly created post.
        accountPage.deletePost();
        Delay(500);
        driver.findElement(By.xpath(logoutXpath)).click();

        logger.info("Finished TC registeredUserCanCreatePublicAndPrivateComment.");
        Delay(500);
        softAssert.assertAll();
    }

    @Test
    public void registeredUserCanLikeAndUnlikePost() throws InterruptedException {

        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        SoftAssert softAssert = new SoftAssert();

        logger.info("Executing TC registeredUserCanLikeAndUnlikePost.");
        Login("James");
        profilePage.selectPrivatePost();
        Delay(500);
        //Create new post.
        profilePage.createPost();
        Delay(1000);
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");
        driver.findElement(By.xpath(logoutXpath)).click();
        Delay(1000);
        Login("Elon Musk");

        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");
        softAssert.assertEquals(profilePage.likesCounter.getText(), "0");
        //Like the newly created post.
        profilePage.LikePost();
        Delay(1000);
        softAssert.assertEquals(profilePage.likesCounter.getText(), "1");
        //Unlike the newly created post.
        profilePage.LikePost();
        Delay(1000);
        softAssert.assertEquals(profilePage.likesCounter.getText(), "0");
        driver.findElement(By.xpath(logoutXpath)).click();

        Login("James");
        driver.findElement(By.xpath("/html/body/div[1]/div/a[2]")).click();
        Delay(1000);
        //Deletes the newly created post.
        accountPage.deletePost();
        Delay(500);
        driver.findElement(By.xpath(logoutXpath)).click();

        logger.info("Finished TC registeredUserCanLikeAndUnlikePost.");
        Delay(500);
        softAssert.assertAll();

    }


    @Test
    public void registeredUserCanCommentPostLikeAndUnlikeComment() throws InterruptedException {

        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        SoftAssert softAssert = new SoftAssert();

        logger.info("Executing TC registeredUserCanLikeAndUnlikePost.");

        //Create new post.
        Login("James");
        profilePage.selectPrivatePost();
        Delay(500);
        profilePage.createPost();
        Delay(1000);
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");

        //Create new comment.
        profilePage.createComment();
        Delay(1000);
        driver.findElement(By.xpath(logoutXpath)).click();

        //Tests like and unlike comment functionality.
        Login("Elon Musk");
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");
        softAssert.assertEquals(profilePage.textComment.getText(), "Selenium created comment.");
        softAssert.assertEquals(profilePage.likesCounterComments.getText(), "0");
        profilePage.likeComment();
        Delay(1000);
        softAssert.assertEquals(profilePage.likesCounterComments.getText(), "1");
        profilePage.likeComment();
        Delay(1000);
        softAssert.assertEquals(profilePage.likesCounterComments.getText(), "0");
        driver.findElement(By.xpath(logoutXpath)).click();

        //Deletes the newly created post with the comment.
        Login("James");
        driver.findElement(By.xpath("/html/body/div[1]/div/a[2]")).click();
        Delay(1000);
        accountPage.deletePost();
        Delay(500);
        driver.findElement(By.xpath(logoutXpath)).click();

        logger.info("Finished TC registeredUserCanCommentPostLikeAndUnlikeComment.");
        Delay(500);
        softAssert.assertAll();
    }

    @Test
    public void registeredUserCanExpandMoreComments() throws InterruptedException {

        //Arrange
        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        SoftAssert softAssert = new SoftAssert();

        logger.info("Executing TC registeredUserCanExpandMoreComments.");

        //Act

        //Create new post.
        Login("James");
        profilePage.selectPrivatePost();
        Delay(500);
        profilePage.createPost();
        Delay(1000);
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "Selenium created post with private security!");

        //Create new comments.
        profilePage.createMultipleComments(3);
        Delay(1000);
        driver.navigate().refresh();
        Delay(1000);
        String divCommentsList = driver.findElement(By.xpath("//div[contains(@id,'comments-list')]")).getText();

        //Assert
        softAssert.assertFalse(divCommentsList.contains("comment 1"));
        profilePage.clickShowMoreComments();
        Delay(1000);
        ScrollPageDown();
        String divCommentsListExpanded = driver.findElement(By.xpath("//div[contains(@id,'comments-list')]")).getText();
        softAssert.assertTrue(divCommentsListExpanded.contains("comment 1"));


        //Deletes the newly created post with the comment.
        Delay(1000);
        driver.findElement(By.xpath("/html/body/div[1]/div/a[2]")).click();
        Delay(1000);
        accountPage.deletePost();
        Delay(500);
        driver.findElement(By.xpath(logoutXpath)).click();

        logger.info("Finished TC registeredUserCanExpandMoreComments.");
        Delay(500);
        softAssert.assertAll();
    }

    @Test
    public void postFeedIsChronological() throws InterruptedException {

        //Arrange
        ProfilePage profilePage = new ProfilePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        SoftAssert softAssert = new SoftAssert();

        logger.info("Executing TC postFeedIsChronological.");

        //Act

        //Create new post.
        Login("James");
        profilePage.selectPrivatePost();
        Delay(500);
        profilePage.createPostMessage("post 1");
        Delay(1000);
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "post 1");
        profilePage.createPostMessage("post 2");
        Delay(1000);
        softAssert.assertEquals(profilePage.textPostPrivate.getText(), "post 2");
        profilePage.clickAccountPage();
        Delay(1000);

        //Assert
        softAssert.assertEquals(accountPage.textPostAccount.getText(), "post 2");
        accountPage.deletePost();
        Delay(1000);
        accountPage.deletePost();
        Delay(1000);
        driver.findElement(By.xpath(logoutXpath)).click();

        logger.info("Finished TC postFeedIsChronological.");
        Delay(500);
        softAssert.assertAll();
    }

}
