package com.socialNetwork.TestCases;

import com.socialNetwork.PageObjects.HomePage;
import com.socialNetwork.PageObjects.LoginPage;
import com.socialNetwork.PageObjects.ProfilePage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TC_Login extends BaseClass {

    //LoginPage loginPage = new LoginPage(driver); -> NEVER INITIALIZE THE PAGE HERE!!! ALWAYS IN THE @TEST !!!

    @Test(dataProvider = "LoginData")
    public void UserCanNOTLoginWithInvalidCredentialsDDT(String username, String password) throws InterruptedException, IOException {

        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);
        ProfilePage profilePage = new ProfilePage(driver);

        homePage.clickHomeLogin();
        loginPage.enterUsername(username);
        logger.info("Username is entered.");
        loginPage.enterPassword(password);
        logger.info("Password is entered.");
        loginPage.clickLogin();
        logger.info("Login button is clicked.");
        Delay(1000);

        if (driver.getTitle().equals("News feed page")){
            Assert.assertEquals(driver.getTitle(), "News feed page");
            logger.info("Login test failed");
            takeScreenShot(driver, "loginTest");
            profilePage.clickLogout();
            Delay(1000);
        } else {
            Assert.assertEquals(driver.getTitle(), "Login");
            logger.info("Login test passed, validation is correct.");
            Delay(1000);
            driver.navigate().to(baseURL);
        }



    }

}
