package com.socialNetwork.TestCases;

import com.socialNetwork.PageObjects.HomePage;
import com.socialNetwork.PageObjects.RegistrationPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TC_Registration extends BaseClass {

    @Test(dataProvider = "RegistrationData")
    public void UserCanNOTRegisterWithInvalidCredentialsDDT(String username, String password, String email) throws InterruptedException, IOException {

        HomePage homePage = new HomePage(driver);
        RegistrationPage registrationPage = new RegistrationPage(driver);

        Delay(1000);
        homePage.clickHomeRegister();
        registrationPage.enterUsername(username);
        logger.info("Username is entered.");
        registrationPage.enterPassword(password);
        logger.info("Password is entered.");
        registrationPage.enterEmail(email);
        logger.info("Email is entered");
        registrationPage.clickRegister();
        logger.info("Button Register is clicked.");
        Delay(1000);

        if (driver.getTitle().equals("Registration page")){
            Assert.assertEquals(driver.getTitle(), "Registration page");
            Assert.assertEquals(registrationPage.getMessageSuccessfulRegistration(), "We sent you an activation email with a link for a confirmation.\n" +
                    "    Please click on the link in the email to activate your account with us");
            logger.info("User can register with invalid credentials!!!");
            takeScreenShot(driver, "registration test");
            Assert.fail();
        } else {
            Assert.assertEquals(driver.getTitle(), "Registration Page");
            logger.info("Registration failed, validation is correct.");
            Delay(1000);
            driver.navigate().to(baseURL);
        }

    }

}
