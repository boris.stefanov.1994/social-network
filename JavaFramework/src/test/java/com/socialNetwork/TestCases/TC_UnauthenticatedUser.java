package com.socialNetwork.TestCases;

import com.socialNetwork.PageObjects.HomePage;
import com.socialNetwork.PageObjects.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TC_UnauthenticatedUser extends BaseClass{

    @Test
    public void UnauthenticatedUserCanNOTseeProfileData() throws InterruptedException, IOException {

        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);

        logger.info("Executing TC UnauthenticatedUserCanNOTseeProfileData");
        homePage.search();
        Delay(1000);
        homePage.clickProfileButton();
        Delay(1000);

        if (driver.getTitle().equals("Login")){
            Assert.assertEquals(driver.getTitle(), "Login");
            logger.info("TC PASSED - Unauthenticated user can NOT see registered user's profile data.");
            driver.navigate().to(baseURL);
        } else {
            Assert.assertNotEquals(driver.getTitle(), "Login");
            logger.info("TC FAILED - Unauthenticated user can see registered user's profile data.");
            takeScreenShot(driver, "Unauthenticated User Can NOT see Profile Data test");
            driver.navigate().to(baseURL);
        }


    }
}
