package com.socialNetwork.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {

    Properties property;

    public ReadConfig() {

        File source = new File("./Configuration/config.properties");
        try {
            FileInputStream fis = new FileInputStream(source);
            property = new Properties();
            property.load(fis);
        } catch (Exception e){
            System.out.println("Exception is " + e.getMessage());
        }
    }

    public String getApplicationURL(){
        String url = property.getProperty("baseURL");
        return url;
    }
    public String goToLoginPage(){
        String loginURL = property.getProperty("loginURL");
        return loginURL;
    }

    public String getLogoutXpath(){
        String xpath = property.getProperty("logout");
        return xpath;
    }

    public String getPassword(){
        String password = property.getProperty("password");
        return password;
    }

    public String getXpathButtonHomeLogin(){
        String xpath = property.getProperty("buttonHomeLogin");
        return xpath;
    }

    public String getXpathButtonLogin(){
        String xpath = property.getProperty("buttonLogin");
        return xpath;
    }

    public String getChromePath(){
        String chromepath = property.getProperty("chromepath");
        return chromepath;
    }

    public String getFireFoxPath(){
        String firefoxpath = property.getProperty("firefoxpath");
        return firefoxpath;
    }


}
