/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 83.8, "KoPercent": 16.2};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.838, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.838, 500, 1500, "HTTP Request"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 500, 81, 16.2, 10.947999999999997, 0, 103, 56.900000000000034, 74.94999999999999, 97.99000000000001, 542.2993492407809, 1660.2621678416485, 65.66906182212581], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["HTTP Request", 500, 81, 16.2, 10.947999999999997, 0, 103, 56.900000000000034, 74.94999999999999, 97.99000000000001, 542.2993492407809, 1660.2621678416485, 65.66906182212581], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["The operation lasted too long: It took 11 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 44 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 96 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 35 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 72 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 17 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 63 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 18 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 45 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 69 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 95 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 36 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 62 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 71 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 67 milliseconds, but should not have lasted longer than 10 milliseconds.", 5, 6.172839506172839, 1.0], "isController": false}, {"data": ["The operation lasted too long: It took 61 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, 3.7037037037037037, 0.6], "isController": false}, {"data": ["The operation lasted too long: It took 91 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, 3.7037037037037037, 0.6], "isController": false}, {"data": ["The operation lasted too long: It took 85 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 37 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 49 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 31 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 19 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 103 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 30 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 90 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 13 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 25 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 97 milliseconds, but should not have lasted longer than 10 milliseconds.", 4, 4.938271604938271, 0.8], "isController": false}, {"data": ["The operation lasted too long: It took 14 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 23 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, 3.7037037037037037, 0.6], "isController": false}, {"data": ["The operation lasted too long: It took 56 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 93 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 89 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 75 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 38 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 24 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 48 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 66 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 98 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 57 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 65 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 74 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 92 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, 3.7037037037037037, 0.6], "isController": false}, {"data": ["The operation lasted too long: It took 58 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 64 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 40 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 94 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 21 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}, {"data": ["The operation lasted too long: It took 100 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 51 milliseconds, but should not have lasted longer than 10 milliseconds.", 2, 2.4691358024691357, 0.4], "isController": false}, {"data": ["The operation lasted too long: It took 46 milliseconds, but should not have lasted longer than 10 milliseconds.", 1, 1.2345679012345678, 0.2], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 500, 81, "The operation lasted too long: It took 67 milliseconds, but should not have lasted longer than 10 milliseconds.", 5, "The operation lasted too long: It took 97 milliseconds, but should not have lasted longer than 10 milliseconds.", 4, "The operation lasted too long: It took 61 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, "The operation lasted too long: It took 91 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, "The operation lasted too long: It took 23 milliseconds, but should not have lasted longer than 10 milliseconds.", 3], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["HTTP Request", 500, 81, "The operation lasted too long: It took 67 milliseconds, but should not have lasted longer than 10 milliseconds.", 5, "The operation lasted too long: It took 97 milliseconds, but should not have lasted longer than 10 milliseconds.", 4, "The operation lasted too long: It took 61 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, "The operation lasted too long: It took 91 milliseconds, but should not have lasted longer than 10 milliseconds.", 3, "The operation lasted too long: It took 23 milliseconds, but should not have lasted longer than 10 milliseconds.", 3], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
