# Social Network

- Team repository for managing framework and documentation for ScaleFocus project.
- Team members: Boris Stefanov and Ivailo Yordanov


**DEV GitLab repository (issues)** - https://gitlab.com/stan.ivanov93/social-media-project/issues?scope=all&utf8=✓&state=all

**Trello** - https://trello.com/b/pWorVGV6/social-network-test-project


**TestRail** - https://counterstrike.testrail.io/index.php?/suites/overview/1

**Bug report** - https://docs.google.com/spreadsheets/d/1gZOmnkJmZQ3LkUEijLqxgcbakCotLq8icYodfkNOLg8/edit#gid=0
