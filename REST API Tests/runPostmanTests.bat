set mypath=%cd%
cd %mypath%
npm install -g newman
npm install -g newman-reporter-htmlextra
newman run SocialNetworkProject.postman_collection.json -e SocialNetwork.postman_environment.json -r htmlextra