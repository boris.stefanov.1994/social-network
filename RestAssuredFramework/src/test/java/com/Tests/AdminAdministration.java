package com.Tests;

import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;

public class AdminAdministration extends BaseClass {

    @Test
    public void adminCanEditUserProfile(){

        HashMap data = new HashMap();
        data.put("alias", "James May");
        data.put("description", "Good news, the new Dacia Sandero...");
        data.put("favoriteMap", "Vertigo");
        data.put("favoriteWeapon", "P90");
        data.put("nationality", "United Kingdom");
        data.put("privacyType", "public");
        data.put("rank", "Silver I");
        data.put("team", "Grand Tour");

        given()
                .auth().basic("Arnold", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+urlAdmin+"/update/James")
                .then()
                .statusCode(200)
                .log().body()
                .body(containsString("James May"))
                .body(containsString("Good news, the new Dacia Sandero..."))
                .body(containsString("Vertigo"))
                .body(containsString("P90"))
                .body(containsString("United Kingdom"))
                .body(containsString("Silver I"))
                .body(containsString("Grand Tour"));
    }



}
