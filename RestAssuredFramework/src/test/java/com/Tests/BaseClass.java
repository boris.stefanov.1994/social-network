package com.Tests;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class BaseClass {

    public  String urlBase;
    public final String urlPost;
    public final String urlNationality;
    public final String urlMap;
    public final String urlConnection;
    public final String urlComment;
    public final String urlRank;
    public final String urlWeapon;
    public final String urlReply;
    public final String urlUser;
    public final String urlAdmin;
    public static String postId = "";
    public static String commentId = "";
    public static String replyId = "";
    public String user;
    public static Map<String, String> constants;

    public BaseClass(){
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("test-base.yaml");
        constants = yaml.load(inputStream);
        System.out.println(constants.get("urlBase"));

        urlBase = constants.get("urlBase");
        urlPost = constants.get("urlPost");
        urlNationality = constants.get("urlNationality");
        urlMap = constants.get("urlMap");
        urlConnection = constants.get("urlConnection");
        urlComment = constants.get("urlComment");
        urlRank = constants.get("urlRank");
        urlWeapon = constants.get("urlWeapon");
        urlReply = constants.get("urlReply");
        urlUser = constants.get("urlUser");
        urlAdmin = constants.get("urlAdmin");
        user = constants.get("user");

    }

    public void createPost(String author, String postText){

        HashMap dataPost1 = new HashMap();
        dataPost1.put("id", "0");
        dataPost1.put("picture", "string");
        dataPost1.put("switchSecurity", "true");
        dataPost1.put("text", postText);

        Response response1 =
                given()
                        .auth().basic(author, "password")
                        .contentType("application/json")
                        .body(dataPost1)
                        .when()
                        .post(urlBase + urlPost + "/create")
                        .then()
                        .statusCode(200)
                        .log().body()
                        .extract().response();

        String jsonString1 = response1.asString();

        postId = response1.then().contentType(ContentType.JSON).extract().path("id").toString();
        System.out.println(postId);
        Assert.assertTrue(jsonString1.contains(postText));

    }

    public void deletePostAuthor(String author){
        //Delete test data
        given()
                .auth().basic(author, "password")
                .when()
                .delete(urlBase + urlPost + "/delete/" + postId)
                .then()
                .statusCode(200);
    }

    public void deletePostAdmin(){
        given()
                .auth().basic("Arnold", "password")
                .when()
                .delete(urlBase + "/api/admin/posts/delete/" + postId)
                .then()
                .statusCode(200);
    }

    public void createComment(String author, String textComment){
        Response responseComment1 =
                given()
                        .auth().basic(author, "password")
                        .contentType("application/json")
                        .body(textComment)
                        .when()
                        .post(urlBase + urlComment + "/create/" + postId)
                        .then()
                        .statusCode(200)
                        .log().body()
                        .extract().response();

        String jsonString4 = responseComment1.asString();

        commentId = String.valueOf(responseComment1.then().contentType(ContentType.JSON).extract().path("id"));

        Assert.assertTrue(jsonString4.contains(textComment));
    }

    public void createReply(String author, String textReply){

        Response responseReply1 =
                given()
                        .auth().basic(author, "password")
                        .contentType("application/json")
                        .body(textReply)
                        .when()
                        .post(constants.get("urlBase") + urlReply + "/comment/create/" + commentId)
                        .then()
                        .statusCode(200)
                        .log().body()
                        .extract().response();

        String jsonString7 = responseReply1.asString();

        replyId = String.valueOf(responseReply1.then().contentType(ContentType.JSON).extract().path("id"));

        Assert.assertTrue(jsonString7.contains(textReply));
    }


}
