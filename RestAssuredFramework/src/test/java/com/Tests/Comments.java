package com.Tests;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;

public class Comments extends BaseClass{

    //Test class with all test cases and request endpoints for Comment functionality

// Tests for Authenticated User

    @Test
    public void verifyCommentBodyCanNOTbeEmpty() {

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Execute test
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .post(urlBase + urlComment + "/create/"+ postId)
                .then()
                .statusCode(400);

        //Delete test data
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void editOwnComment(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment and get commentID
        createComment("Elon Musk", "authenticated user comment");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", commentId);
        data.put("text", "edited comment by author of the comment");

        given()
                .auth().basic("Elon Musk", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+urlComment+"/update")
                .then()
                .statusCode(200)
                .log().body()
                .body("text", equalTo("edited comment by author of the comment"));

        //Delete test data
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void verifyCommentCanNOTbeEditedWithEmptyBody() {

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Elon Musk", "authenticated user comment");

        HashMap data = new HashMap();
        data.put("id", commentId);
        data.put("text", "");

        given()
                .auth().basic("Elon Musk", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase + urlComment + "/update")
                .then()
                .statusCode(500);

        //Delete test data
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void likeComment() {

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Elon Musk", "authenticated user comment");

        //Execute test
       Integer responseBody =  given()
                .auth().basic("Sherlock", "password")
                .when()
                .put(urlBase + urlComment + "/like/"+commentId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);
       Assert.assertTrue(responseBody == 1);

        //Delete test data
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void getNumberOfLikesForComments(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Elon Musk", "authenticated user comment");

        //Like comment
        Integer responseBody =  given()
                .auth().basic("Sherlock", "password")
                .when()
                .put(urlBase + urlComment + "/like/" + commentId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);
        Assert.assertTrue(responseBody == 1);

        //Execute test
      Integer responseBodyLikes =  given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase + urlComment + "/like/count/" + commentId)
                .then()
                .statusCode(200)
              .extract().body().as(Integer.class);

      Assert.assertTrue(responseBodyLikes == 1);

        //Delete test data
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void getCommentsForPost(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Elon Musk", "authenticated user comment");

        //Execute test
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase + urlComment + "/"+ postId)
                .then()
                .statusCode(200)
                .body(containsString("authenticated user comment"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");

    }

    @Test
    public void deleteOwnComment(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Elon Musk", "authenticated user comment");

        //Execute test
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .delete(urlBase + urlComment + "/delete/" + commentId)
                .then()
                .statusCode(200);

        //Verify comment is deleted with additional GET request by author of the comment
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase + urlComment + "/"+postId)
                .then()
                .statusCode(200)
                .body("comments", is(0));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    // Tests for Admin User; Arnold is admin!

    @Test
    public void adminEditComment(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Sherlock", "Sherlock comments this post");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", commentId);
        data.put("text", "admin edited comment");

        given()
                .auth().basic("Arnold", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+"/api/admin/comments/update")
                .then()
                .statusCode(200)
                .log().body()
                .body("text", equalTo("admin edited comment"));

        //Verify comment is edited by Admin with additional GET request
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase + urlComment + "/"+postId)
                .then()
                .statusCode(200)
                .log().body()
                .body(containsString("admin edited comment"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void verifyAdminCanNOTEditCommentWithEmptyBody(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Sherlock", "Sherlock comments this post");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", commentId);
        data.put("text", "");

        given()
                .auth().basic("Arnold", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+"/api/admin/comments/update")
                .then()
                .statusCode(500);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void adminGetCommentsByPost(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Sherlock", "Sherlock comments this post");


        //Execute test
        given().
                auth().basic("Arnold", "password")
                .when()
                .get(urlBase+"/api/admin/comments/post/"+postId)
                .then()
                .statusCode(200)
                .body(containsString("Sherlock comments this post"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void adminDeleteComment(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Sherlock", "Sherlock comments this post");


        //Execute test
        given()
                .auth().basic("Arnold", "password")
                .when()
                .delete(urlBase + "/api/admin/comments/delete/" + commentId)
                .then()
                .statusCode(200);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    // Tests for Unauthenticated User

    @Test
    public void unauthenticatedUserCanSeeCommentsForPost(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get a commentID
        createComment("Sherlock", "Sherlock comments this post");

        //Execute test
        given()
                .auth().none()
                .when()
                .get(urlBase + "/api/comments/post/"+postId)
                .then()
                .statusCode(200)
                .body(containsString("Sherlock comments this post"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void unauthenticatedUserCanGetCommentById(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Execute test
        given()
                .auth().none()
                .when()
                .get(urlBase + "/api/comments/"+commentId)
                .then()
                .statusCode(200)
                .body("text", equalTo("I must break you!"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }


}
