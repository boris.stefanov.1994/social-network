package com.Tests;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Connections extends BaseClass {

    @Test(priority = 1)
    public void sendConnectionRequest(){

        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .post(urlBase+urlConnection+"/send/"+user)
                .then()
                .statusCode(200)
                .body(containsString("WAITING"));
    }

    @Test(priority = 2)
    public void getPendingUsers(){

        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase+urlConnection+"/waiting")
                .then()
                .statusCode(200);
    }

    @Test(priority = 3)
    public void acceptFriendRequest(){

        given()
                .auth().basic("Sherlock", "password")
                .when()
                .put(urlBase+urlConnection+"/accept/1")
                .then()
                .statusCode(200)
                .body(containsString("true"));
    }

    @Test(priority = 4)
    public void getAllConnectedUsers(){

        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase+urlConnection+"/approved")
                .then()
                .statusCode(200)
                .body(containsString("Elon Musk"))
                .body(containsString("Sherlock"));
    }

    @Test(priority = 5)
    public void disconnectWithUsers(){

        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .delete(urlBase+urlConnection+"/disconnect/"+user)
                .then()
                .statusCode(200);
    }
    @Test(priority = 6)
    public void verifyUserIsDisconnected(){

        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase+urlConnection+"/approved")
                .then()
                .statusCode(200)
                .body(not(containsString("Sherlock")));
    }


}
