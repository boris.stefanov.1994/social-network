package com.Tests;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Map extends BaseClass {

    @Test
    public void getAllMaps(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlMap)
                .then()
                .statusCode(200)
                .body(containsString("Dust 2"))
                .body(containsString("Inferno"));
    }

    @Test
    public void getMapById(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlMap+"/1")
                .then()
                .statusCode(200)
                .body("name", equalTo("Dust 2"));
    }

    @Test
    public void getMapByName(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlMap+"/name/dust 2")
                .then()
                .statusCode(200)
                .body("name", equalTo("Dust 2"));
    }

}
