package com.Tests;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Nationality extends BaseClass {

    @Test
    public void getAllNationalities(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlNationality)
                .then()
                .statusCode(200)
                .body(containsString("Bulgaria"))
                .body(containsString("United States"));
    }

    @Test
    public void getNationalityById(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlNationality+"/20")
                .then()
                .statusCode(200)
                .body("name", equalTo("Romania"));
    }

    @Test
    public void getNationalityByName(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlNationality+"/name/France")
                .then()
                .statusCode(200)
                .body("name", equalTo("France"));
    }

}
