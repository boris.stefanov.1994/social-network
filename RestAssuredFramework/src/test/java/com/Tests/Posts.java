package com.Tests;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Posts extends BaseClass {

    //Test class with all test cases and request endpoints for Post functionality

    // Tests for Authenticated User

    @Test
    public void verifyPostBodyCanNOTbeEmpty() {

        HashMap data = new HashMap();
        data.put("id", "0");
        data.put("picture", "string");
        data.put("switchSecurity", "true");
        data.put("text", "");

        given()
                .auth().basic("Rambo", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .post(urlBase + urlPost + "/create")
                .then()
                .statusCode(400);
    }

    @Test
    public void getPostByID() {

        //Set up
        createPost("Rambo", "Counter-Strike Global Offensive is modern version of the great classic!");

        //Execute test
        Response response =
                given()
                        .auth().basic("Rambo", "password")

                        .when()
                        .get(urlBase + urlPost + "/" + postId)
                        .then()
                        .statusCode(200)
                        .log().body()
                        .extract().response();

        String jsonString = response.asString();

        Assert.assertTrue(jsonString.contains("Counter-Strike Global Offensive is modern version of the great classic!"));

        //Tear down
        deletePostAuthor("Rambo");

    }

    @Test
    public void likePost() {

        //Create post and get postID
        createPost("Rambo", "Counter-Strike Global Offensive is modern version of the great classic!");

        //Execute test
        Integer responseBody = given()
                .auth().basic("Sherlock", "password")
                .when()
                .put(urlBase + urlPost + "/like/" + postId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);
        Assert.assertTrue(1 == responseBody);

        //Delete test data
        deletePostAuthor("Rambo");
    }

    @Test
    public void likePostWithAnotherUser() {

        //Create post and get postID
        createPost("Rambo", "Counter-Strike Global Offensive is modern version of the great classic!");

        //Execute test
        Integer responseBody = given()
                .auth().basic("Sherlock", "password")
                .when()
                .put(urlBase + urlPost + "/like/" + postId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);
        Assert.assertTrue(1 == responseBody);

        Integer responseBody2 = given()
                .auth().basic("Elon Musk", "password")
                .when()
                .put(urlBase + urlPost + "/like/" + postId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);

        Assert.assertTrue(2 == responseBody2);

        //Delete test data
        deletePostAuthor("Rambo");
    }

    @Test
    public void getCountsOfLikesForPost() {

        //Create post and get postID
        createPost("Rambo", "Counter-Strike Global Offensive is modern version of the great classic!");

        //Execute test

        //Like comment
        Integer responseBody = given()
                .auth().basic("Sherlock", "password")
                .when()
                .put(urlBase + urlPost + "/like/" + postId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);
        Assert.assertTrue(1 == responseBody);

        //Like comment with another user
        Integer responseBody2 = given()
                .auth().basic("Elon Musk", "password")
                .when()
                .put(urlBase + urlPost + "/like/" + postId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);

        Assert.assertTrue(2 == responseBody2);

        //Get all likes for this post
        Integer responseBody3 = given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "/like/count/" + postId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);

        Assert.assertTrue(2 == responseBody3);

        //Delete test data
        deletePostAuthor("Rambo");
    }

    @Test
    public void getPostByPopularity() {

        given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "/mine?page=0&size=3")
                .then()
                .statusCode(200);
    }

    @Test
    public void updateOwnPost() {

        //Create post and get postID
        createPost("Rambo", "Counter-Strike Global Offensive is modern version of the great classic!");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", postId);
        data.put("switchSecurity", "false");
        data.put("text", "edit own post");

        given()
                .auth().basic("Rambo", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase + urlPost + "/update")
                .then()
                .statusCode(200)
                .log().body()
                .body("text", equalTo("edit own post"));

        //Verify post is edited
        given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "/" + postId)
                .then()
                .statusCode(200)
                .log().body()
                .body("text", equalTo("edit own post"));

        //Delete test data
        deletePostAuthor("Rambo");
    }

    @Test
    public void verifyPostCanNOTbeEditedWithEmptyBody() {

        //Create post and get postID
        createPost("Rambo", "Counter-Strike Global Offensive is modern version of the great classic!");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", postId);
        data.put("switchSecurity", "false");
        data.put("text", "");

        given()
                .auth().basic("Rambo", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase + urlPost + "/update")
                .then()
                .statusCode(500);

        //Delete test data
        deletePostAuthor("Rambo");

        //Verify post is deleted
        given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "/" + postId)
                .then()
                .statusCode(404);
    }

    @Test
    public void getPostsPerPage() {

        given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "?page=0&size=1")
                .then()
                .statusCode(200);
    }

    @Test
    public void getPostsForCurrentUser() {

        given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "/mine?page=0&size=1")
                .then()
                .statusCode(200);
    }

    @Test
    public void getPostsForUser() {

        given()
                .auth().basic("Sherlock", "password")
                .when()
                .get(urlBase + urlPost + "/user/Sherlock?page=1&size=1")
                .then()
                .statusCode(200)
                .body(containsString("Admin edited post!"));
    }

    // Tests for Unauthenticated User

    @Test
    public void getAllPosts() {

        given()
                .auth().none()
                .when()
                .get(urlBase + "/api/posts")
                .then()
                .statusCode(200);
    }

    @Test
    public void getPostsById() {

        //Create post and get postID
        createPost("Sherlock", "Check out my TV series 'Sherlock'. Season 4 is coming. Prepare to solve murders... #detective");

        //Execute test
        given()
                .auth().none()
                .when()
                .get(urlBase + "/api/posts/" + postId)
                .then()
                .statusCode(200)
                .body("text", equalTo("Check out my TV series 'Sherlock'. Season 4 is coming. Prepare to solve murders... #detective"));

        //Delete test data
        deletePostAuthor("Sherlock");
    }

    // Tests for Admin User; Arnold is admin!

    @Test
    public void adminEditPost() {

        //Create post and get postID
        createPost("Sherlock", "Check out my TV series 'Sherlock'. Season 4 is coming. Prepare to solve murders... #detective");

        HashMap data = new HashMap();
        data.put("id", postId);
        data.put("picture", "string");
        data.put("switchSecurity", "true");
        data.put("text", "Admin edited post!");

        //Execute test
        given()
                .auth().basic("Arnold", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase + "/api/admin/posts/update")
                .then()
                .statusCode(200)
                .log().body()
                .body("text", equalTo("Admin edited post!"));

        //Verify post is edited by admin with additional GET request from author of the post
        given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "/" + postId)
                .then()
                .statusCode(200)
                .log().body()
                .body("text", equalTo("Admin edited post!"));

        //Delete test data from Admin account
        deletePostAdmin();

        //Verify post is deleted by Admin from author of the post with GET request
        given()
                .auth().basic("Rambo", "password")
                .when()
                .get(urlBase + urlPost + "/" + postId)
                .then()
                .statusCode(404);
    }

    @Test
    public void verifyAdminCanNOTEditPostWithEmptyBody() {

        //Create post and get postID
        createPost("Sherlock", "Check out my TV series 'Sherlock'. Season 4 is coming. Prepare to solve murders... #detective");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", postId);
        data.put("picture", "string");
        data.put("switchSecurity", "true");
        data.put("text", "");

        given()
                .auth().basic("Arnold", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase + "/api/admin/posts/update")
                .then()
                .statusCode(500);

        //Delete test data from Admin account
        deletePostAdmin();
    }

    @Test
    public void adminGetPostsByPage() {

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase + "/api/admin/posts?page=1&size=1")
                .then()
                .statusCode(200);
    }

}
