package com.Tests;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Rank extends BaseClass {

    @Test
    public void getAllRanks(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlRank)
                .then()
                .statusCode(200)
                .body(containsString("Silver I"))
                .body(containsString("Legendary Eagle Master"));
    }

    @Test
    public void getRankById(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlRank+"/1")
                .then()
                .statusCode(200)
                .body("name", equalTo("Silver I"));
    }

    @Test
    public void getRankByName(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlRank+"/name/Silver I")
                .then()
                .statusCode(200)
                .body("name", equalTo("Silver I"));
    }

}
