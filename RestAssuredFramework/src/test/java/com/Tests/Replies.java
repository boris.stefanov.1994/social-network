package com.Tests;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Replies extends BaseClass {

    // Tests for Authenticated User

    //Test class with all test cases and request endpoints for Replies functionality

    @Test
    public void verifyReplyBodyCanNOTbeEmpty() {

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Execute test
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .post(urlBase + urlReply + "/comment/create/"+ commentId)
                .then()
                .statusCode(400);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void editOwnReply(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        HashMap data = new HashMap();
        data.put("id", replyId);
        data.put("text", "edited reply by author of the reply");

        //Execute test
        given()
                .auth().basic("Elon Musk", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+urlReply+"/update")
                .then()
                .statusCode(200)
                .log().body()
                .body("text", equalTo("edited reply by author of the reply"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void verifyReplyCanNOTbeEditedWithEmptyBody() {

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", replyId);
        data.put("text", "");

        given()
                .auth().basic("Elon Musk", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase + urlReply + "/update")
                .then()
                .statusCode(500);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void likeReply() {

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        Integer responseBody = given()
                .auth().basic("Sherlock", "password")
                .when()
                .put(urlBase + urlReply + "/like/"+replyId)
                .then()
                .statusCode(200)
                .extract().body().as(Integer.class);

        Assert.assertTrue(responseBody == 1);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void getAllRepliesForAComment() {

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Create second reply for the same post
        createReply("James", "James May replies to a comment");

        //Execute test
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase + urlReply + "/comment/"+commentId)
                .then()
                .statusCode(200)
                .body(containsString("this is a reply to a comment from authenticated user"))
                .body(containsString("James May replies to a comment"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void deleteOwnReply(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .delete(urlBase + urlReply + "/delete/" + replyId)
                .then()
                .statusCode(200);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    // Tests for Admin User; Arnold is admin!

    @Test
    public void adminGetRepliesForComment(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        given().
                auth().basic("Arnold", "password")
                .when()
                .get(urlBase+"/api/admin/replies/comment/"+commentId)
                .then()
                .statusCode(200)
                .body(containsString("this is a reply to a comment from authenticated user"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void adminEditReply(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", replyId);
        data.put("text", "admin edited this reply for a comment of a post");

        given()
                .auth().basic("Arnold", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+"/api/admin/replies/update")
                .then()
                .statusCode(200)
                .log().body()
                .body(containsString("admin edited this reply for a comment of a post"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    @Test
    public void verifyAdminCanNOTEditCommentWithEmptyBody(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        HashMap data = new HashMap();
        data.put("id", replyId);
        data.put("text", "");

        given()
                .auth().basic("Arnold", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+"/api/admin/replies/update")
                .then()
                .statusCode(500);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");

    }

    @Test
    public void adminDeleteReply(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        given()
                .auth().basic("Arnold", "password")
                .when()
                .delete(urlBase + "/api/admin/replies/delete/" + replyId)
                .then()
                .statusCode(200);

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

    // Tests for Unauthenticated User

    @Test
    public void unauthenticatedUserCanSeeRepliesForComment(){

        //Create post and get postID
        createPost("Elon Musk", "2020 Moon landing mission with live people on board, has just started! :o SpaceX is on the verge of changing modern science!");

        //Create comment for a post and get CommentID
        createComment("Rocky Balboa", "I must break you!");

        //Create reply for a comment of a post and get CommentID
        createReply("Elon Musk", "this is a reply to a comment from authenticated user");

        //Execute test
        given()
                .auth().none()
                .when()
                .get(urlBase + "/api/replies/comment/"+commentId)
                .then()
                .statusCode(200)
                .body(containsString("this is a reply to a comment from authenticated user"));

        //Delete post by author of the post
        deletePostAuthor("Elon Musk");
    }

}
