package com.Tests;

import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class UserAdministration extends BaseClass {

    @Test
    public void getCurrentUser(){

        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase+urlUser+"/current")
                .then()
                .statusCode(200)
                .body("id", equalTo(1))
                .body("username", equalTo("Elon Musk"))
                .body("email", equalTo("elon@test.com"))
                .body("alias", equalTo("Tesla"));
    }

    @Test
    public void getAllUsers(){

        given()
                .auth().basic("Elon Musk", "password")
                .when()
                .get(urlBase+urlUser)
                .then()
                .statusCode(200)
                .body(containsString("Elon Musk"))
                .body(containsString("Rambo"))
                .body(containsString("Rocky Balboa"))
                .body(containsString("Sherlock"))
                .body(containsString("James"))
                .body(containsString("Arnold"));
    }

    @Test
    public void editOwnProfile(){

        HashMap data = new HashMap();
        data.put("alias", "Tesla");
        data.put("description", "I am the real Tony Stark!");
        data.put("favoriteMap", "dust 2");
        data.put("favoriteWeapon", "M4A4");
        data.put("nationality", "United States");
        data.put("privacyType", "private");
        data.put("rank", "Silver I");
        data.put("team", "SpaceX");

        given()
                .auth().basic("Elon Musk", "password")
                .contentType("application/json")
                .body(data)
                .when()
                .put(urlBase+urlUser)
                .then()
                .statusCode(200)
                .log().body()
                .body(containsString("Tesla"))
                .body(containsString("I am the real Tony Stark!"))
                .body(containsString("Dust 2"))
                .body(containsString("M4A4"))
                .body(containsString("United States"))
                .body(containsString("Silver I"))
                .body(containsString("SpaceX"));
    }

    @Test
    public void deleteOwnProfile(){
        given()
                .auth().basic("James2", "password")
                .when()
                .delete(urlBase+urlUser+"/delete")
                .then()
                .statusCode(200)
                .body(not(containsString("James2")));
    }

    @Test
    public void verifyUserIsDeleted(){

        given()
                .auth().basic("James2", "password")
                .when()
                .get(urlBase+urlUser+"/current")
                .then()
                .statusCode(401);
    }

}
