package com.Tests;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Weapons extends BaseClass {

    @Test
    public void getAllWeapons(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlWeapon)
                .then()
                .statusCode(200)
                .body(containsString("Glock-18"))
                .body(containsString("R8 Revolver"));
    }

    @Test
    public void getWeaponById(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlWeapon+"/20")
                .then()
                .statusCode(200)
                .body("name", equalTo("M4A4"));
    }

    @Test
    public void getWeaponByName(){

        given()
                .auth().basic("Arnold", "password")
                .when()
                .get(urlBase+urlWeapon+"/name/G3SG1")
                .then()
                .statusCode(200)
                .body("name", equalTo("G3SG1"));
    }

}
